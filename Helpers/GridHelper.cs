﻿#region NameSpaces
using System.Collections.Generic;
#endregion
#region Common.Helpers
namespace Common.Helpers
{
    #region GridHelper
    public class GridHelper<T>
    {
        public List<T> ListOfItems { get; set; }
        public int TotalRecordCount { get; set; }
    }
    #endregion

    #region GridSearchParameters
    public class GridSearchParameters
    {
        public string currentPage { get; set; }
        public string pageItems { get; set; }
        public string filterBy { get; set; }
        public string filterByFields { get; set; }
        public string orderBy { get; set; }
        public string orderByReverse { get; set; }
        
    }
    #endregion

    #region DropDownObject
    public class DropDownObject
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public string Amount { get; set; }
    }
    #endregion
}

#endregion
