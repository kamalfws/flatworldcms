﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlatworldCMS.Helpers
{
    public enum EnumModule
    {
        User,
        Project,
        Birthday,
        News,
        Announcement,
        Photo,
        Banner,
        Video,
        Event,
        Document
    }

    public enum EnumAction
    {
        created,
        updated,
        deleted,
        activated,
        deactivated
    }
}