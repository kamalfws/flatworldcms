﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace FlatworldCMS.Helpers
{
    public static class Slack
    {
        public static void PostMessage(EnumModule module,EnumAction action,string Name, string userName)
        {
            try
            {
                string message = string.Empty;

                string slackAccessTokenURL = ConfigurationManager.AppSettings["SlackAccessTokenURL"].ToString();
                string slackChannel = ConfigurationManager.AppSettings["SlackChannel"].ToString();
               
                switch (module.ToString())
                {
                    case "Document":
                        message = "*Document:'" + Name+ "'* document is created, have a look on it! It might help you!  \n http://www.geniecircle.com/";
                        break;
                    case "User":
                        message = "Let’s welcome our new Flatworld Genie  *'" + Name+ "'* to our family  \n http://www.geniecircle.com/";
                        break;
                    case "Project":
                        message = "Heads  up for the new  project *'"+Name+ "'*! \n http://www.geniecircle.com/";
                        break;
                    case "Birthday":
                        message = "*Birthday:* Let’s wish *'" + Name+ "'* a happy birthday! Wishing you a great year ahead! \n http://www.geniecircle.com/";
                        break;
                    case "News":
                        message = "*News:* Read some news about *'" + Name + "'*, few might be important \n http://www.geniecircle.com/";
                        break;
                    case "Announcement":
                        message = "*Announcement: '" + Name+ "'* is ON! Check it! \n http://www.geniecircle.com/";
                        break;
                    case "Photo":
                        message = "*Photo:* Check out our *'" + Name + "'* for a glimpse of the special moments. \n http://www.geniecircle.com/";
                        break;
                    case "Video":
                        message = "*Video:* If you are too lazy to glance through the photos, spend few minutes on video album *'" + Name + "'* \n http://www.geniecircle.com/";
                        break;
                    case "Event":
                        message = "*Event:* Work while work, Play while play! Join us on *'" + Name + "'* \n http://www.geniecircle.com/";
                        break;
                    default:
                        message = "";
                        break;
                }

                //title = string.IsNullOrEmpty(title) ? string.Empty : title.Trim();
                userName = string.IsNullOrEmpty(userName) ? "FlatworldCMSBOT" : userName.Trim();
                //string message = module.ToString() + ": '" + title.ToString() + "' has been " + action.ToString();



                SlackClient client = new SlackClient(slackAccessTokenURL);

                client.PostMessage(username: userName,
                           text: message,
                           channel: slackChannel);
            }
            catch (Exception ex)
            {

            }
        }

    }

    public class SlackClient
    {
        private readonly Uri _uri;
        private readonly Encoding _encoding = new UTF8Encoding();

        public SlackClient(string urlWithAccessToken)
        {
            _uri = new Uri(urlWithAccessToken);
        }

        //Post a message using simple strings  
        public void PostMessage(string text, string username = null, string channel = null)
        {
            Payload payload = new Payload()
            {
                Channel = channel,
                Username = username,
                Text = text
            };

            PostMessage(payload);
        }

        //Post a message using a Payload object  
        public void PostMessage(Payload payload)
        {
            string payloadJson = JsonConvert.SerializeObject(payload);

            using (WebClient client = new WebClient())
            {
                NameValueCollection data = new NameValueCollection();
                data["payload"] = payloadJson;

                var response = client.UploadValues(_uri, "POST", data);

                //The response text is usually "ok"  
                string responseText = _encoding.GetString(response);
            }
        }
    }

    #region LoggedCompanyInfo
    public class LoggedCompanyInfo
    {
        #region LoggedCompanyInfo
        private LoggedCompanyInfo()
        {

        }
        #endregion

        #region CompanyId
        public Int32 CompanyId { get; set; }
        #endregion

        //#region UserRole
        //public string UserRole { get; set; }
        //#endregion

        #region SESSION_SINGLETON
        private const string SESSION_SINGLETON = "SINGLETON";
        #endregion

        #region GetCurrentSingleton
        public static LoggedCompanyInfo GetCurrentSingleton()
        {
            LoggedCompanyInfo oSingleton;
            if (System.Web.HttpContext.Current != null)
            {
                if (null == System.Web.HttpContext.Current.Session[SESSION_SINGLETON])
                {
                    //No current session object exists, use private constructor to 
                    // create an instance, place it into the session
                    oSingleton = new LoggedCompanyInfo();
                    System.Web.HttpContext.Current.Session[SESSION_SINGLETON] = oSingleton;
                    System.Web.HttpContext.Current.Session.Timeout = 120;
                }
                else
                {
                    //Retrieve the already instance that was already created
                    oSingleton = (LoggedCompanyInfo)System.Web.HttpContext.Current.Session[SESSION_SINGLETON];
                }
            }
            else
            {
                oSingleton = new LoggedCompanyInfo();
            }

            //Return the single instance of this class that was stored in the session
            return oSingleton;
        }
        #endregion
    }
    #endregion

    //This class serializes into the Json payload required by Slack Incoming WebHooks  
    public class Payload
    {
        [JsonProperty("channel")]
        public string Channel { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }
}