﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FWSCommonCode;
using System.Net.Mail;
using System.DirectoryServices;
using FlatworldCMS.Infrastructure;
using FlatworldCMS.Database;
using System.Configuration;
using System.IO;

namespace FlatworldCMS
{
    public class MvcApplication : System.Web.HttpApplication
    {
       
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        #region EmailErrorAttribute
        public class EmailErrorAttribute : HandleErrorAttribute
        {
            public override void OnException(ExceptionContext filterContext)
            {

                filterContext.ExceptionHandled = true;

                //ExceptionLogger objException = FactoryException.getExceptionObject(FactoryException.ExceptionType.Email);

                //objException.UserName = "Administrator";
                //objException.EmailClass = new MailUtilities();

                //objException.EmailClass.EmailFrom = "abhishek.h@flatworldsolutions.com";
                //objException.EmailClass.EmailTo = "abhishek.h@flatworldsolutions.com";

                //objException.EmailClass.SenderEmailServer = "secure.emailsrvr.com";
                //objException.EmailClass.SenderEmailID = "noreply@flatworldsolutions.com";
                //objException.EmailClass.SenderEmailPassword = "N0r#lyfw";
                //objException.EmailClass.EmailSubject = "Exception encountered";
                Exception ex = filterContext.Exception;
                //objException.Log(ex);
                Models.ErrorLog.Logger(ex);
                ViewResult view = new ViewResult();
                view.ViewName = "Error";
                filterContext.Result = view;

            }
        }
        #endregion

       

        #region FactoryException
        public class FactoryException
        {
            #region ExceptionType
            public enum ExceptionType { NotePad, Email, DataBase }
            #endregion

            #region getExceptionObject
            public static ExceptionLogger getExceptionObject(ExceptionType exceptionType)
            {
                ExceptionLogger objException = null;
                switch (exceptionType)
                {
                    case ExceptionType.NotePad: objException = new TextLogger(); break;
                    case ExceptionType.Email: objException = new EmailLogger(); break;
                    case ExceptionType.DataBase: objException = new DBLogger(); break;
                }
                return objException;
            }
            #endregion
        }
        #endregion


        #region ValidateUser
        //public class ValidateUser : ActionFilterAttribute, IActionFilter
        //{
        //    void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        //    {

        //        if (ValidateCredentials())
        //        {
        //            this.OnActionExecuting(filterContext);
        //        }
        //        else
        //        {
        //            var url = new UrlHelper(filterContext.RequestContext);
        //            var loginUrl = url.Content("~/Home/Unauthorized");
        //            filterContext.HttpContext.Response.Redirect(loginUrl, true);
                   
        //        }

               

        //    }

        //    protected void Logger(string msg)
        //    {
        //        string fileName = ConfigurationManager.AppSettings["LogFilePath"].ToString() + "ZohoRecordsSync\\";
        //        if (!System.IO.Directory.Exists(fileName))
        //            System.IO.Directory.CreateDirectory(fileName);

        //        FileStream file = new FileStream(fileName + "log_" + DateTime.Now.ToString("dd_MMMM_yyyy") + ".txt", FileMode.Append, FileAccess.Write);
        //        StreamWriter writer = new StreamWriter(file);
        //        writer.WriteLine(DateTime.Now + " " + msg);
        //        writer.Close();
        //        file.Close();
        //        file.Dispose();
        //        writer.Dispose();
        //    }

        //    private bool ValidateCredentials()
        //    {
        //        string userName = Environment.UserName;

        //        DirectoryEntry entry = new DirectoryEntry();

        //        string Email = string.Empty;
        //        // get a DirectorySearcher object
        //        DirectorySearcher search = new DirectorySearcher(entry);

        //        // specify the search filter
        //        search.Filter = "(&(objectClass=user)(anr=" + userName + "))";

        //        // specify which property values to return in the search
        //        search.PropertiesToLoad.Add("givenName");   // first name
        //        search.PropertiesToLoad.Add("sn");          // last name
        //        search.PropertiesToLoad.Add("mail");        // smtp mail address
               
        //        // perform the search
        //        SearchResult result = search.FindOne();
        //        if (result != null)
        //        {
        //            Email = result.Properties["mail"][0].ToString();
                  
        //            if (EmployeeDetails(Email))
        //            {
        //                return true;
        //            }
        //            else
        //            {
        //                return true;
        //            }
        //        }
        //        else
        //        {
        //            return true;
        //        }

        //    }

        //    private bool EmployeeDetails(string Email)
        //    {
        //        UnitOfWork unitOfWork;
        //        unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

        //        User dbUser = unitOfWork.User.Find(m => m.Email == Email && m.IsActiveinIntranet == true && m.Active == true).FirstOrDefault();

        //        if (dbUser != null)
        //            return true;
        //        else
        //            return false;
        //    }
        //}
        #endregion
    }
}
