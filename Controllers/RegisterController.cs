﻿using Common.Helpers;
using FlatworldCMS.Database;
using FlatworldCMS.Helpers;
using FlatworldCMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FlatworldCMS.Controllers
{
   public class RegisterController : Controller
    {
              
        public ActionResult Register()
        {
            return View();
        }
               
        public JsonResult UserRegistration(String CompanyName,string CompanyEmail,string Address, string Zipcode, string FirstName, string Mobile, string UserEmail)
        {
            try
            {
                FlatworldCMSEntities context = new FlatworldCMSEntities();
                ObjectParameter output = new ObjectParameter("Result", typeof(int));
                context.User_Registration(CompanyName,CompanyEmail,Address,Zipcode,FirstName,Mobile,UserEmail,output);
                return Json("success");
            }
            catch (Exception ex)
            {
                return Json("error");
            }
        }
        
    }
}