﻿using Common.Helpers;
using FlatworldCMS.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FlatworldCMS.Controllers
{
    public class SettingsController : Controller
    {
        public ActionResult Settings()
        {
            return View();
        }

        #region GetAllSettings
        public JsonResult GetAllSettings(GridSearchParameters SettingSearch)
        {
            List<object> lst = new List<object>();
            try
            {
                FlatworldCMSEntities context = new FlatworldCMSEntities();
                GridHelper<ApplicationConfig_GetList_Result> App_ConfigList = new GridHelper<ApplicationConfig_GetList_Result>();
                List<ApplicationConfig_GetList_Result> lstAllSettings = context.ApplicationConfig_GetList(SettingSearch.filterBy, Convert.ToInt32(SettingSearch.currentPage), Convert.ToInt32(SettingSearch.pageItems), SettingSearch.orderBy, SettingSearch.orderByReverse).ToList();
                App_ConfigList.ListOfItems = lstAllSettings;
                if (lstAllSettings.Count > 0)
                    App_ConfigList.TotalRecordCount = (int)lstAllSettings[0].TotalCount;
                lst.Add(App_ConfigList);
                List<DropDownObject> Companies = new List<DropDownObject>();
                Companies.Add(new DropDownObject() { Text = "Select", Value = "Select" });
                foreach (Company c in context.Companies.OrderBy(c => c.Name))
                {
                    Companies.Add(new DropDownObject() { Text = c.Name, Value = c.ID.ToString() });
                }
                lst.Add(Companies);
            }
            catch (Exception ex)
            {
                
            }
            //return lst;
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        #endregion 

        #region CrudOperation
        public bool CrudOperation(ApplicationConfiguration SettingEntity)
        {
            try
            {
                FlatworldCMSEntities context = new FlatworldCMSEntities();
                ObjectParameter output = new ObjectParameter("Result", typeof(int));
                context.ApplicationConfig_CrudOperations(SettingEntity.CompanyID, SettingEntity.HostName, SettingEntity.UserName, SettingEntity.Password,
SettingEntity.EnableSSL, SettingEntity.PortNo, SettingEntity.SlackIntegration, SettingEntity.ZohoProjects, output);
                return ((int)output.Value == 9999);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

       

    }
}
