﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Security.Principal;
using System.Threading;
using System.DirectoryServices.Protocols;
using System.IO;
using System.DirectoryServices;

namespace FlatworldCMS.Controllers
{
    public class BaseController : Controller
    {

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
           
            if (ValidateCredentials())
            {
                //send them off to the login page
                var url = new UrlHelper(filterContext.RequestContext);
                var loginUrl = url.Content("~/Home/About");
                filterContext.HttpContext.Response.Redirect(loginUrl, true);
            }
            else
            {
                    var url = new UrlHelper(filterContext.RequestContext);
                    var loginUrl = url.Content("~/Home/Unauthorized");
                    filterContext.HttpContext.Response.Redirect(loginUrl, true);
            }

            base.OnActionExecuting(filterContext);
        }

        private bool ValidateCredentials()
        {
            string userName = Environment.UserName;

            DirectoryEntry entry = new DirectoryEntry();

            string Email = string.Empty;
            // get a DirectorySearcher object
            DirectorySearcher search = new DirectorySearcher(entry);

            // specify the search filter
            search.Filter = "(&(objectClass=user)(anr=" + userName + "))";

            // specify which property values to return in the search
            search.PropertiesToLoad.Add("givenName");   // first name
            search.PropertiesToLoad.Add("sn");          // last name
            search.PropertiesToLoad.Add("mail");        // smtp mail address

            // perform the search
            SearchResult result = search.FindOne();
            if (result != null)
            {
                Email = result.Properties["mail"][0].ToString();
                if (EmployeeDetails(Email))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        private bool EmployeeDetails(string Email)
        {
            Session["EmployeeID"] = "1134";
            return true;
        }
        
    }
}