﻿using Common.Helpers;
using FlatworldCMS.Database;
using FlatworldCMS.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FlatworldCMS.Controllers
{
    public class HomeController : Controller
    {
        int hrs, mins, srNo = 0;
        TimeSpan tSpan;
        string timeZoneCookie;

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        #region GetCompanies
        public JsonResult GetCompanies()
        {
                 List<DropDownObject> Companies = new List<DropDownObject>();
            try
            {
                FlatworldCMSEntities context = new FlatworldCMSEntities();
                Companies.Add(new DropDownObject() { Text = "Select", Value = "Select" });
                foreach (Company st in context.Companies.OrderBy(s => s.Name))
                {
                    Companies.Add(new DropDownObject() { Text = st.Name, Value = st.ID.ToString() });
                }
                
                return Json(Companies, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(Companies, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion


        #region SaveUserSelectedCompany
        public bool SaveUserSelectedCompany(Int32 CompanyId)
        {
            try
            {
                LoggedCompanyInfo.GetCurrentSingleton().CompanyId = CompanyId;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        [MvcApplication.EmailErrorAttribute]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CreateTimezoneSession(string timeZone)
        {
            Response.Cookies["CMS_UserTimezone"].Value = timeZone;
            Response.Cookies["CMS_UserTimezone"].Expires = DateTime.UtcNow.AddDays(1);
            return Json("");
        }

        /// <summary>
        /// To calculate Timezone difference
        /// </summary>
        public void GetTimezoneDiff(ref int hrs, ref int mins, ref TimeSpan tSpan)
        {
            if (Request.Cookies["CMS_UserTimezone"] != null)
            {
                timeZoneCookie = Request.Cookies["CMS_UserTimezone"].Value.ToString();
                hrs = Convert.ToInt32(timeZoneCookie.Substring(0, 3));
                mins = Convert.ToInt32(timeZoneCookie.Substring(3, 2));
                tSpan = new TimeSpan(hrs, mins, 0);
            }
        }
    }
}