﻿using FlatworldCMS.Database;
using FlatworldCMS.Helpers;
using FlatworldCMS.Infrastructure;
using FlatworldCMS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FlatworldCMS.Controllers
{
    [MvcApplication.EmailErrorAttribute]
    public class AnnouncementController : Controller
    {
        UnitOfWork unitOfWork;
        int hrs, mins;
        string timeZoneCookie;
        int compId = LoggedCompanyInfo.GetCurrentSingleton().CompanyId;
        public JsonResult GetCategories()
        {
            var categories = GetAllCategories().Select(ac => new { Id = ac.Id, Name = ac.Name }).ToList();


            return Json(categories, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveAnnouncement(int categoryId, string title, string description, int id)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            if (id == 0)
            {
                Announcement announcementObj = new Announcement { CategoryId = categoryId, Description = description, Title = title, CreatedOn = DateTime.UtcNow, CreatedBy = AuthenticatedUser.GetLoggedInCustomerDetails().Id, IsActive = true };
                unitOfWork.Announcement.Add(announcementObj);
                Slack.PostMessage(EnumModule.Announcement, EnumAction.created, title, User.Identity.Name);
            }
            else
            {
                Announcement announcementObj = unitOfWork.Announcement.Find(a => a.Id == id).FirstOrDefault();
                announcementObj.CategoryId = categoryId;
                announcementObj.Description = description;
                announcementObj.Title = title;
                announcementObj.ModifiedOn = DateTime.UtcNow;
                announcementObj.ModifiedBy = AuthenticatedUser.GetLoggedInCustomerDetails().Id;
                Slack.PostMessage(EnumModule.Announcement, EnumAction.updated, title, User.Identity.Name);
            }
            unitOfWork.Complete();

            return Json("success");
        }

        [HttpPost]
        [HandleError]
        public JsonResult SaveAnnouncementCategory(string name, int categoryId = 0)
        {
           
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            if (unitOfWork.AnnouncementCategory.Find(ac => ac.Name == name.Trim() && ac.Id != categoryId && ac.Company== compId).Any())
                return Json("Category Exists");

            var category = new AnnouncementCategory();

            if (categoryId > 0)
                category = unitOfWork.AnnouncementCategory.Find(c => c.Id == categoryId && c.Company==compId).FirstOrDefault();
            category.Name = name;

            if (categoryId == 0)
            {
                category.CreatedOn = DateTime.UtcNow;
                category.CreatedBy = AuthenticatedUser.GetLoggedInCustomerDetails().Id;
                unitOfWork.AnnouncementCategory.Add(category);
            }
            else
            {
                category.ModifiedBy = AuthenticatedUser.GetLoggedInCustomerDetails().Id;
                category.ModifiedOn = DateTime.UtcNow;
            }

            unitOfWork.Complete();
            return Json("Success");
        }

        [HttpGet]
        public JsonResult GetAnnonuncements()
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var categories = new List<AnnouncementCategory>();
            var query = unitOfWork.Announcement.Find(a => 1 == 1);
            if (AuthenticatedUser.GetLoggedInCustomerDetails().Role != "Administrator")
            {
                categories = GetAllCategories(true);
                query = query.Where(a => a.IsActive && a.AnnouncementCategory.IsActive && a.AnnouncementCategory.Company== compId);
            }
            else
                categories = GetAllCategories();
            GetTimezoneDiff();

            var categoryList = categories.Select(c => new { Id = c.Id, Name = c.Name, IsActive = c.IsActive }).ToList();
            var announcements = query.OrderByDescending(a => a.CreatedOn).Select(a => new { Id = a.Id, Title = a.Title, FullDescription = a.Description, Description = a.Description.Length > 100 ? (a.Description.Substring(0, 100).Replace("\n", "<br/>") + "...") : a.Description.Replace("\n", "<br/>"), CategoryId = a.CategoryId, CreatedBy = a.CreatedBy != null ? (a.User.FirstName + " " + a.User.LastName) : "", Category = a.AnnouncementCategory.Name, CreatedOn = a.CreatedOn.AddHours(hrs).AddMinutes(mins).ToString("dd MMM yyyy, hh:mm tt"), IsActive = a.IsActive }).ToList();
            return Json(new { categories = categoryList, announcements = announcements }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UploadAnnouncementsPhoto(HttpPostedFileBase file)
        {
            if (Request.Files.Count > 0)
            {
                file = Request.Files[0];
                string extension = System.IO.Path.GetExtension(file.FileName);
                string ImgPath = "~/Images/Announcements/";
                string fname = DateTime.Now.ToString("yyyyddmmhhmmssss");
                string path = System.IO.Path.Combine(Server.MapPath(ImgPath), fname + extension);
                if (!Directory.Exists(Server.MapPath(ImgPath)))
                    Directory.CreateDirectory(Server.MapPath(ImgPath));
                file.SaveAs(path);
                return Json("../../Images/Announcements/" + fname + extension);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ChangeCategoryStatus(int id)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var category = unitOfWork.AnnouncementCategory.Find(ac => ac.Id == id).FirstOrDefault();
            category.IsActive = !category.IsActive;
            unitOfWork.Complete();
            return Json("Success");
        }

        [HttpPost]
        public JsonResult ChangeStatus(int id)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            Announcement announcement = unitOfWork.Announcement.Find(a => a.Id == id).FirstOrDefault();
            announcement.IsActive = !announcement.IsActive;
            announcement.ModifiedOn = DateTime.UtcNow;
            unitOfWork.Complete();
            return Json("success");
        }

        private List<AnnouncementCategory> GetAllCategories(bool isActive = false)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var categoryQuery = unitOfWork.AnnouncementCategory.Find(c => 1 == 1);
            if (isActive)
                categoryQuery = categoryQuery.Where(c => c.IsActive && c.Company== compId);
            var categories = categoryQuery.OrderBy(ac => ac.Name).ToList();
            return categories;
        }

        /// <summary>
        /// To calculate Timezone difference
        /// </summary>
        public void GetTimezoneDiff()
        {
            if (Request.Cookies["CMS_UserTimezone"] != null)
            {
                timeZoneCookie = Request.Cookies["CMS_UserTimezone"].Value.ToString();
                hrs = Convert.ToInt32(timeZoneCookie.Substring(0, 3));
                mins = Convert.ToInt32(timeZoneCookie.Substring(3, 2));
            }
        }

    }
}