﻿using FlatworldCMS.Database;
using FlatworldCMS.Helpers;
using FlatworldCMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FlatworldCMS.Controllers
{
    [MvcApplication.EmailErrorAttribute]
    public class EventsController : Controller
    {
        UnitOfWork unitOfWork;
        int hrs, mins;
        string timeZoneCookie;
        int compId = LoggedCompanyInfo.GetCurrentSingleton().CompanyId;
        // GET: Events
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetEvents(string date)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

            DateTime selectedDate;
            if (!string.IsNullOrEmpty(date))
                selectedDate = Convert.ToDateTime(date);
            else
                selectedDate = DateTime.Now;
            GetTimezoneDiff();
            var eventObjs = unitOfWork.Events.GetAll().Where(e=>e.Company==compId).OrderByDescending(m => m.StartDate)
                 .Select(m => new
                 {
                     Name = m.Name,
                     Description = m.Description,
                     EndDate = m.EndDate,
                     EventId = m.EventId,
                     Image = m.Image,
                     Location = m.Location,
                    // StartDate = m.StartDate.AddHours(hrs).AddMinutes(mins),
                     StartDate = m.StartDate,
                     //StartDateFormat = (m.StartDate).ToUniversalTime().ToString("dd/MM/yyyy"),
                     StartDateFormat = (m.StartDate.AddHours(hrs).AddMinutes(mins)),
                     CreatedOn = m.CreatedOn,
                     Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(m.StartDate.Month),
                     year = m.StartDate.Year


                 }).ToList();

            return Json(new { events = eventObjs, year = DateTime.Now.Year }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult SaveEvent(HttpPostedFileBase file, string eventName, string location, string Description, DateTime startDate, int? Id)
        {
            try
            {
                string relativePath = string.Empty;
                string slackMessage = string.Empty;
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                if (file != null)
                {
                    var directoryPath = Server.MapPath("~/Events");
                    if (!Directory.Exists(directoryPath))
                        Directory.CreateDirectory(directoryPath);

                    string fileExtension = file.FileName.Split('.')[1];

                    relativePath = "~/Events/" + DateTime.Now.Ticks + "." + fileExtension;

                    file.SaveAs(Server.MapPath(relativePath));
                }

                Event eventObj = new Event();

                if (Id != null)
                {
                    eventObj = unitOfWork.Events.Find(e => e.EventId == Id).FirstOrDefault();

                    if (eventObj != null)
                    {
                        eventObj.Name = eventName;
                        eventObj.CreatedBy = 0;
                        eventObj.Company = compId;
                        eventObj.CreatedOn = DateTime.UtcNow;
                        eventObj.Description = Description;
                        eventObj.EndDate = startDate;
                        if (file != null)
                            eventObj.Image = relativePath.Replace("~", ConfigurationManager.AppSettings["ApplicationUrl"].ToString());
                        eventObj.Location = location;
                        eventObj.StartDate = startDate;

                    }
                    slackMessage = "";
                   
                }
                else
                {
                    eventObj.Name = eventName;
                    eventObj.CreatedBy = 0;
                    eventObj.CreatedOn = DateTime.UtcNow;
                    eventObj.Company = compId;
                    eventObj.Description = Description;
                    eventObj.EndDate = startDate;
                    eventObj.Image = relativePath.Replace("~", ConfigurationManager.AppSettings["ApplicationUrl"].ToString());
                    eventObj.Location = location;
                    eventObj.StartDate = startDate;

                    unitOfWork.Events.Add(eventObj);
                    slackMessage = "";
                }
                Slack.PostMessage(EnumModule.Event, EnumAction.created, eventName, User.Identity.Name);
                unitOfWork.Complete();

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteEvent(Int32 eventId)
        {
            try
            {
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

                var eventObj = unitOfWork.Events.Find(d => d.EventId == eventId).FirstOrDefault();

                if (eventObj != null)
                {
                    if (eventObj.Image != null)
                    {
                        RemoveEventImage(eventObj.Image);
                    }
                    unitOfWork.Events.Remove(eventObj);
                    unitOfWork.Complete();
                }

                return Json("Success");

            }
            catch (Exception ex)
            {
                return Json("Error");

            }
        }

        private void RemoveEventImage(string fileName)
        {
            var directoryPath = Server.MapPath("~/Events/");
            string[] paths = fileName.Split('/');
            string filename = paths[paths.Count() - 1];
            string path = fileName.Replace(ConfigurationManager.AppSettings["ApplicationUrl"].ToString(), "");

            path = Path.Combine(directoryPath, filename);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
        }


        /// <summary>
        /// To calculate Timezone difference
        /// </summary>
        public void GetTimezoneDiff()
        {
            if (Request.Cookies["CMS_UserTimezone"] != null)
            {
                timeZoneCookie = Request.Cookies["CMS_UserTimezone"].Value.ToString();
                hrs = Convert.ToInt32(timeZoneCookie.Substring(0, 3));
                mins = Convert.ToInt32(timeZoneCookie.Substring(3, 2));
            }
        }
    }
}