﻿using Common.Helpers;
using FlatworldCMS.Database;
using FlatworldCMS.Helpers;
using FlatworldCMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FlatworldCMS.Controllers
{
    [MvcApplication.EmailErrorAttribute]
    public class UserController : Controller
    {
        UnitOfWork unitOfWork;
        int hrs, mins;
        string timeZoneCookie;
        TimeSpan tSpan;

        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        //  [MvcApplication.ValidateUser]
        [MvcApplication.EmailError]
        [HttpPost]
        public JsonResult GetUsers(GridSearchParameters userSearch)
        {
            try
            {
                var c = LoggedCompanyInfo.GetCurrentSingleton().CompanyId;
                FlatworldCMSEntities context = new FlatworldCMSEntities();

                List<Users_GetList_Result> users = new List<Users_GetList_Result>();

                users = context.Users_GetList(userSearch.filterBy,LoggedCompanyInfo.GetCurrentSingleton().CompanyId, Convert.ToInt32(userSearch.currentPage), Convert.ToInt32(userSearch.pageItems), userSearch.orderBy, userSearch.orderByReverse).ToList();               

                return Json(users, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EnableDisableUser(int userID, bool enable)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

            User dbUser = unitOfWork.User.Find(m => m.UserId == userID).FirstOrDefault();

            dbUser.IsActiveinIntranet = enable;

            unitOfWork.Complete();

            return Json("Success", JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetSingleUser(int userID)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            //GetTimezoneDiff();
            var dbUser = unitOfWork.User.Find(m => m.UserId == userID).Select(u => new
            {
                UserId = u.UserId,
                FirstName = u.FirstName,
                LastName = u.LastName,
                Mobile = u.Mobile,
                //DateofBirth = u.DateofBirth.AddHours(hrs).AddMinutes(mins),
                //DateofJoining = u.DateofJoining.AddHours(hrs).AddMinutes(mins),
                DateofBirth = u.DateofBirth,
                DateofJoining = u.DateofJoining,
                Role = u.Role,
                Department = u.Department,
                Designation = u.Designation,
                Email = u.Email,
                ReportingManager = u.ReportingManager,
                ContactAddress = u.ContactAddress==null?string.Empty:u.ContactAddress,
                ProfilePicPath = u.ProfilePicPath
            }).FirstOrDefault();


            var Roles = unitOfWork.RoleMaster.GetAll().Select(r => new { Id = r.RoleCode, RoleDescription = r.RoleDescription }).ToList();

            var designations = unitOfWork.DesignationMaster.GetAll().Where(d => d.Active).Select(d => new { Id = d.DesgId, Name = d.DesgName }).ToList();

            var departments = unitOfWork.DepartmentMaster.GetAll().Where(d => d.Active == null || d.Active.Value).Select(d => new { Id = d.DeptId, Name = d.DeptName }).ToList();

            var managers = unitOfWork.User.Find(u => u.IsActiveinIntranet != null && u.IsActiveinIntranet.Value).Select(u => new { Id = u.UserId, Name = (u.FirstName + " " + u.LastName).Trim() + " " + u.UserId.ToString().PadLeft(4, '0') }).OrderBy(u => u.Name).ToList();

            return Json(new { user = dbUser, roles = Roles, designations = designations, departments = departments, managers = managers }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveUser(int Id, string FirstName, string LastName, string Email, string Mobile, DateTime DOB, DateTime DOJ, string Manager, string Gender, string Department, string Designation, string Role, bool isNew)
        {
            int compId = LoggedCompanyInfo.GetCurrentSingleton().CompanyId;
            try
            {
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                User user = new User();
                if (!isNew)
                    user = unitOfWork.User.Find(u => u.UserId == Id || u.Email==Email).FirstOrDefault();

                if (isNew && unitOfWork.User.Find(u => u.UserId == Id).Count()!=0)
                    return Json("Employee Number Exists");

                if (unitOfWork.User.Find(u => u.UserId != Id && u.Email == Email).Count()!=0)
                    return Json("Email Exists");

                if (unitOfWork.User.Find(u => u.UserId != Id && u.Mobile == Mobile).Count() != 0)
                    return Json("Mobile No Exists");
                //GetTimezoneDiff();
                user.UserId = Id;
                user.FirstName = FirstName;
                user.LastName = LastName;
                user.Email = Email;
                user.Mobile = Mobile;
                //user.DateofBirth = DOB.Subtract(tSpan);
                //user.DateofJoining = DOJ.Subtract(tSpan);
                user.DateofBirth = DOB.ToUniversalTime();
                user.DateofJoining = DOJ;
                user.ReportingManager = Manager;
                user.Gender = Gender == null ? "M" : Gender;
                user.Department = Department;
                user.Designation = Designation;
                user.Role = Role;
                user.CreatedDate = DateTime.UtcNow;
                user.CreatedBy = 1;
                user.UserType = "Permanent";
                user.Active = true;
                user.Company = compId;
                if (isNew)
                {
                    unitOfWork.User.Add(user);
                    Slack.PostMessage(EnumModule.User, EnumAction.created, FirstName + " " + LastName, User.Identity.Name);
                }
                else {//Slack.PostMessage(EnumModule.User, EnumAction.updated, FirstName + " " + LastName, User.Identity.Name);
                }
                unitOfWork.Complete();

                return Json("success");
            }
            catch (Exception ex)
            {
                return Json("error");
            }


        }

        [HttpPost]
        public JsonResult UpdateProfile(int Id, string FirstName, string LastName, string Mobile, DateTime DOB, string Address, HttpPostedFileBase ProfilePicFile)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var directoryPath = Server.MapPath("~/ProfilePictures");
            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);
            var profilePicUrl = string.Empty;

            if (ProfilePicFile != null)
            {
                var ticks = DateTime.Now.Ticks;
                string fileExtension = ProfilePicFile.FileName.Split('.')[1];

                profilePicUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
        Request.ApplicationPath + "ProfilePictures/Pic" + ticks + "." + fileExtension;

                ProfilePicFile.SaveAs(directoryPath + "/Pic" + ticks + "." + fileExtension);
            }
            //GetTimezoneDiff();
            var user = unitOfWork.User.Find(u => u.UserId == Id).FirstOrDefault();
            user.FirstName = FirstName;
            user.LastName = LastName;
            user.Mobile = Mobile;
            user.DateofBirth = DOB;
            //user.DateofBirth = DOB.Subtract(tSpan);
            user.ContactAddress = Address;
            if (!string.IsNullOrEmpty(profilePicUrl))
                user.ProfilePicPath = profilePicUrl;
            unitOfWork.Complete();
            return Json("success");
        }

        public JsonResult GetBirthdays()
        {
            try
            {
                int compId = LoggedCompanyInfo.GetCurrentSingleton().CompanyId;

                FlatworldCMSEntities context = new FlatworldCMSEntities();

                List<GetBirthdayDetails_Result> birthdays = new List<GetBirthdayDetails_Result>();

                birthdays = context.GetBirthdayDetails(0,LoggedCompanyInfo.GetCurrentSingleton().CompanyId).ToList();              
                return Json(new { birthdays = birthdays, year = DateTime.Now.Year }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            { return Json(null, JsonRequestBehavior.AllowGet); }
            }


        /// <summary>
        /// Creates User Timezone session and gets difference between UTC and users timezone
        /// </summary>
        /// <param name="timeZone">Time zone(date)</param>
        /// <returns></returns>
        public ActionResult CreateTimezoneSession(string timeZone)
        {
            Response.Cookies["Med_UserTimezone"].Value = timeZone;
            Response.Cookies["Med_UserTimezone"].Expires = DateTime.UtcNow.AddDays(1);
           // GetTimezoneDiff();
            return Json("");
        }

        /// <summary>
        /// To calculate Timezone difference
        /// </summary>
        //public void GetTimezoneDiff()
        //{
        //    if (Request.Cookies["Med_UserTimezone"] != null)
        //    {
        //        timeZoneCookie = Request.Cookies["Med_UserTimezone"].Value.ToString();
        //        hrs = Convert.ToInt32(timeZoneCookie.Substring(0, 3));
        //        mins = Convert.ToInt32(timeZoneCookie.Substring(3, 2));
        //        tSpan = new TimeSpan(hrs, mins, 0);
        //    }
        //}


    }
}