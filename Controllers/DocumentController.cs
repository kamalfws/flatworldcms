﻿using FlatworldCMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlatworldCMS.Database;
using Common.Helpers;
using FlatworldCMS.Helpers;

namespace FlatworldCMS.Controllers
{
    [MvcApplication.EmailErrorAttribute]
    public class DocumentController : Controller
    {
        UnitOfWork unitOfWork;
        int compId = LoggedCompanyInfo.GetCurrentSingleton().CompanyId;
        // GET: Document
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SaveDocument(HttpPostedFileBase file, string title)
        {
            try
            {
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                var directoryPath = Server.MapPath("~/Documents");
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                string fileExtension = file.FileName.Split('.')[1];

                string path = directoryPath + "\\Documents" + DateTime.Now.Ticks + "." + fileExtension;

                file.SaveAs(directoryPath + "\\Documents" + DateTime.Now.Ticks + "." + fileExtension);

                DocumentCategory docCatObj = new DocumentCategory();

                docCatObj.Name = title;

                unitOfWork.DocumentCategory.Add(docCatObj);
                unitOfWork.Complete();

                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

                string fileType = string.Empty;
                if (file.ContentType.Contains("image"))
                    fileType = "Image";
                else if (file.ContentType.Contains("audio"))
                    fileType = "Audio";
                else if (file.ContentType.Contains("video"))
                    fileType = "Video";
                else if (fileExtension.Contains("doc"))
                    fileType = "Word";
                else if (fileExtension.Contains("xls"))
                    fileType = "Excel";
                else if (fileExtension.Contains("ppt"))
                    fileType = "PowerPoint";
                else if (fileExtension == "zip")
                    fileType = "Zip";
                else if (fileExtension == "pdf")
                    fileType = "Pdf";

                Document docObj = new Document();

                docObj.DocumentCategory = docCatObj.Id;

                docObj.Path = path;
                docObj.DocumentName = file.FileName;
                docObj.CreatedOn = DateTime.UtcNow;
                docObj.DocumentType = fileType;
                unitOfWork.Document.Add(docObj);
                unitOfWork.Complete();

               Slack.PostMessage(EnumModule.Document, EnumAction.created, file.FileName, User.Identity.Name);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckCategory(string name)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

            var cat = unitOfWork.DocumentCategory.Find(d => d.Name == name && d.Company==compId).FirstOrDefault();

            if (cat == null)
            {
                return Json("True", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("False", JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult SaveMultipleDocuments(HttpPostedFileBase[] files,int categoryID,string categoryName)
        {
            try
            {
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                var directoryPath = Server.MapPath("~/Documents");
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
                int category = categoryID;
                if (Request.Cookies["category"] != null)
                {
                    category = Convert.ToInt32(Request.Cookies["category"].Value);
                }
             
                foreach (HttpPostedFileBase file in files)
                {
                    unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                    string fileExtension = file.FileName.Split('.')[1];

                    string path = directoryPath + "\\Documents" + DateTime.Now.Ticks + "." + fileExtension;
                    file.SaveAs(directoryPath + "\\Documents" + DateTime.Now.Ticks + "." + fileExtension);
                    string fileType = string.Empty;
                    if (file.ContentType.Contains("image"))
                        fileType = "Image";
                    else if (file.ContentType.Contains("audio"))
                        fileType = "Audio";
                    else if (file.ContentType.Contains("video"))
                        fileType = "Video";
                    else if (fileExtension.Contains("doc"))
                        fileType = "Word";
                    else if (fileExtension.Contains("xls"))
                        fileType = "Excel";
                    else if (fileExtension.Contains("ppt"))
                        fileType = "PowerPoint";
                    else if (fileExtension == "zip")
                        fileType = "Zip";
                    else if (fileExtension == "pdf")
                        fileType = "Pdf";
                    Document docObj = new Document()
                    {
                        DocumentCategory = category,
                        Path = path,
                        DocumentName = file.FileName,
                        CreatedOn = DateTime.UtcNow,
                        DocumentType = fileType
                    };
                    unitOfWork.Document.Add(docObj);
                    unitOfWork.Complete();
                    Slack.PostMessage(EnumModule.Document, EnumAction.updated, file.FileName, User.Identity.Name);
                }
          

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDocCategories()
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

            var catObjs= unitOfWork.DocumentCategory.GetAll().Where(d=>d.Company==compId)
                 .Select(m => new 
                 {
                     Category = m.Name,
                     Id = m.Id
                   
                 }).OrderByDescending(m => m.Id).ToList();

            return Json(catObjs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteDocuments(Int32 categoryId)
        {
            try
            {
                var directoryPath = Server.MapPath("~/Documents");

                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

                var docs = unitOfWork.Document.GetAll().Where(d => d.DocumentCategory == categoryId).Select(m => new { FilePath = m.Path, id = m.DocumentID }).ToList();

                if (docs != null)
                {
                    for (int i = 0; i < docs.Count(); i++)
                    { Int64 docID = docs[i].id;
                        if (System.IO.File.Exists(docs[i].FilePath))
                            System.IO.File.Delete(docs[i].FilePath);
                        Document doc = unitOfWork.Document.Find(f => f.DocumentID == docID).FirstOrDefault();
                        unitOfWork.Document.Remove(doc);
                    }
                }

                DocumentCategory docCat = unitOfWork.DocumentCategory.Find(i => i.Id == categoryId).FirstOrDefault();
                unitOfWork.DocumentCategory.Remove(docCat);
                unitOfWork.Complete();


                return Json("Success");

            }
            catch (Exception ex)
            {
                return Json("Error");

            }
       }

       
        //[MvcApplication.EmailError]
        [HttpPost]
        public JsonResult GetDocuments(GridSearchParameters docsSearch)
       {
            try
            {
                int category = 0;
                //if(Request.Cookies["category"]!=null)
                //{
                    category =Convert.ToInt32( docsSearch.filterByFields);
               // }
                
            FlatworldCMSEntities context = new FlatworldCMSEntities();

            List<Documents_GetList_Result> docs = new List<Documents_GetList_Result>();

                docs = context.Documents_GetList(category, LoggedCompanyInfo.GetCurrentSingleton().CompanyId, docsSearch.filterBy, Convert.ToInt32(docsSearch.currentPage), Convert.ToInt32(docsSearch.pageItems), docsSearch.orderBy, docsSearch.orderByReverse).ToList();
            

            return Json(docs, JsonRequestBehavior.AllowGet);
        }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteSingleDocuments(Int32 docId)
        {
            try
            {              
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

                var docs = unitOfWork.Document.Find(d => d.DocumentID == docId).FirstOrDefault();

                if (docs != null)
                {
                    if(System.IO.File.Exists(docs.Path))
                        System.IO.File.Delete(docs.Path);
                        unitOfWork.Document.Remove(docs);
                }              
                unitOfWork.Complete();


                return Json("Success");

            }
            catch (Exception ex)
            {
                return Json("Error");

            }
        }

        public FileResult Download(string filePath, string fileName)
        {
            if (System.IO.File.Exists(filePath))
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
               
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public JsonResult CheckFile(string filePath)
        {
            if (System.IO.File.Exists(filePath))
            {
                return Json("true");
            }

            else
            { return Json("false"); }

            }
        }
}