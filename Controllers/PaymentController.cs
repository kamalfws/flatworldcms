﻿using FlatworldCMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stripe;
using System.Threading.Tasks;
using FlatworldCMS.Models;

namespace FlatworldCMS.Controllers
{

    [MvcApplication.EmailErrorAttribute]
    public class PaymentController : Controller
    {
        UnitOfWork unitOfWork;

        // GET: Payment
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetPlans()
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var plans = unitOfWork.Plan.GetAll().OrderBy(i => i.ID);
            return Json(plans, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdatePaymentPlan()
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var plans = unitOfWork.Plan.GetAll().OrderBy(i => i.ID);
            return Json(plans, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult Charge()
        //{

        //    return View(new StripeChargeModel());
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Charge(StripeChargeModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    var chargeId = await ProcessPayment(model);
        //    return View();
        //}

        //private async Task<string> ProcessPayment(StripeChargeModel model)
        //{
        //    try
        //    {
        //        return await Task.Run(() =>
        //        {
        //            var myCharge = new StripeChargeCreateOptions
        //            {
        //                // convert the amount of £12.50 to pennies i.e. 1250
        //                Amount = (int)(model.Amount * 100),
        //                Currency = "gbp",
        //                Description = "Description for test charge",

        //                SourceTokenOrExistingSourceId = model.Token
        //            };

        //            var chargeService = new StripeChargeService("sk_test_twofIRKVAAAccGD0B4TvuJbO");
        //            var stripeCharge = chargeService.Create(myCharge);

        //            return stripeCharge.Id;
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        }
}