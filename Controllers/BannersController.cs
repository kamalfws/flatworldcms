﻿using FlatworldCMS.Database;
using FlatworldCMS.Helpers;
using FlatworldCMS.Infrastructure;
using FlatworldCMS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace FlatworldCMS.Controllers
{
    [MvcApplication.EmailErrorAttribute]
    public class BannersController : Controller
    {
        
        UnitOfWork unitOfWork;
        public JsonResult SaveBanner(HttpPostedFileBase file, string title)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var directoryPath = Server.MapPath("~/Banners");
            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            var ticks = DateTime.Now.Ticks;
            string fileExtension = file.FileName.Split('.')[1];

            var fileUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
    Request.ApplicationPath + "Banners/Banner" + ticks + "." + fileExtension;

            file.SaveAs(directoryPath + "/Banner" + ticks + "." + fileExtension);

            var bannerObj = new Banner { ImageUrl = fileUrl, Title = title, IsActive = true, CreatedOn = DateTime.UtcNow };
            unitOfWork.Banner.Add(bannerObj);
            unitOfWork.Complete();
           Slack.PostMessage(EnumModule.Banner, EnumAction.created, title, User.Identity.Name);

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public JsonResult Dashboard()
        {
            var companyid = LoggedCompanyInfo.GetCurrentSingleton().CompanyId;
            try
            {
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                DashboardModel dashboard = new DashboardModel();

                dashboard.Announcements = unitOfWork.Announcement.Find(m => m.IsActive == true)
                    .Join(unitOfWork.User.GetAll(), a => a.CreatedBy, u => u.UserId, (a, u) => new { a, u })
                    .Join(unitOfWork.AnnouncementCategory.GetAll(), ua => ua.a.CategoryId, ac => ac.Id, (ua, ac) => new { ua, ac }).Where(u => u.ac.Company == companyid)
                    .OrderByDescending(m => m.ua.a.CreatedOn).Take(5)
                    .Select(m => new AnnouncememtModel { Id = m.ua.a.Id, Title = m.ua.a.Title, AnnouncedOn = m.ua.a.CreatedOn.ToString("dd MMM yyyy"), AnnouncedBy = (m.ua.u.FirstName + " " + m.ua.u.LastName), BriefDescription = m.ua.a.Description.Length > 50 ? (m.ua.a.Description.Substring(0, 50) + "...") : m.ua.a.Description, Description = m.ua.a.Description, Category = m.ac.Name })
                    .ToList();

                dashboard.Projects = unitOfWork.Project.Find(m => m.Active == true).Where(u=>u.Company==companyid)
                    .OrderByDescending(m => m.CreatedDate).Take(5)
                    .Select(m => new ProjectModel { Id = m.ZohoProjectID, Name = m.Name, Owner = m.Owner, Description = m.Description }).ToList();

                dashboard.Birthdays = unitOfWork.User.Find(m => m.IsActiveinIntranet == true)
                    .Where(m => m.DateofBirth.Month == DateTime.Now.Month && m.Company==companyid)
                    .OrderByDescending(m => m.DateofBirth.Month).Take(5)
                    .Select(m => new BirthdayModel { Name = m.FirstName + " " + m.LastName, BirthDate = m.DateofBirth.ToString("dd MMM yyyy"), ProfilePicPath = m.ProfilePicPath, Designation = m.Designation, Email = m.Email, Phone = m.Mobile }).OrderByDescending(m => m.BirthDate).Take(5).ToList();

                dashboard.News = unitOfWork.News.Find(m => m.IsActive == true)
                    .Join(unitOfWork.User.GetAll(), a => a.CreatedBy, u => u.UserId, (a, u) => new { a, u })
                    .Join(unitOfWork.NewsCategory.GetAll(), ua => ua.a.CategoryId, ac => ac.Id, (ua, ac) => new { ua, ac })
                    .OrderByDescending(m => m.ua.a.CreatedOn).Take(5)
                    .Select(m => new NewsModel { Id=m.ua.a.Id, Title = m.ua.a.Title, Description = m.ua.a.Description.Length > 50 ? (m.ua.a.Description.Substring(0, 50) + "...") : m.ua.a.Description, CreatedBy = (m.ua.u.FirstName + " " + m.ua.u.LastName), CategoryId = m.ua.a.CategoryId, Category = m.ac.Name , CreatedOn = m.ua.a.CreatedOn.ToString("dd MMM yyyy") }).OrderByDescending(m => m.Id).Take(5).ToList();

                dashboard.Events = unitOfWork.Events.GetAll().Where(u=>u.Company==companyid)
                    .OrderByDescending(m => m.CreatedOn).Take(5)
                    .Select(m => new EventModel { Name = m.Name, Location = m.Location, Description = m.Description.Length > 50 ? (m.Description.Substring(0, 50) + "...") : m.Description, EventDate = m.StartDate.ToString("dd MMM yyyy"), Image = m.Image }).OrderByDescending(m => m.EventDate).Take(5).ToList();

                dashboard.Documents = unitOfWork.Document.GetAll()
                    .Join(unitOfWork.DocumentCategory.GetAll(), a => a.DocumentCategory, u => u.Id, (a, u) => new { a, u }).Where(i=>i.u.Company==companyid)
                    .OrderByDescending(m => m.a.CreatedOn)
                    .GroupBy(m => new { Name = m.u.Name, Id = m.u.Id }).Take(5)
                    .Select(m => new DocumentModel { Title = m.Key.Name, Id = m.Key.Id, Count = m.Count() }).ToList();


                dashboard.Videos = unitOfWork.PhotoNVideo.Find(m => m.IsVideo == true)
                        .Join(unitOfWork.Album.Find(m => m.IsVideo == true), p => p.AlbumId, a => a.AlbumId, (p, a) => new { p, a }).Where(i=>i.a.CompanyID==companyid)
                        .OrderByDescending(m => m.p.ID).Take(2)
                        .Select(m => new VideoModel { Path = m.p.Path, Name = string.IsNullOrWhiteSpace(m.p.Name) ? "No Name" : m.p.Name })
                        .ToList();



                dashboard.Photos = unitOfWork.Album.Find(m => m.IsVideo == false).Where(u=>u.CompanyID==companyid)
                                        .OrderByDescending(m => m.UploadedDate).Take(2)
                                        .Select(m => new PhotoModel { Path = m.AlbumPath, Name = m.AlbumName }).ToList();

                return Json(dashboard, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ErrorLog.Logger(ex);
                return Json("false");
            }
        }

        public JsonResult GetBanners()
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var banners = unitOfWork.Banner.Find(b => !b.IsDeleted).Select(b => new { Id = b.Id, ImageUrl = b.ImageUrl, Title = b.Title, IsActive = b.IsActive, IsDeleted = b.IsDeleted }).ToList();

            return Json(banners, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetActiveBanners()
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var banners = unitOfWork.Banner.Find(b => !b.IsDeleted && b.IsActive).Select(b => new { Id = b.Id, ImageUrl = b.ImageUrl, Title = b.Title, IsActive = b.IsActive, IsDeleted = b.IsDeleted }).ToList();

            return Json(banners, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteBanner(int id)
        {

            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var banner = unitOfWork.Banner.Find(b => b.Id == id).FirstOrDefault();
            banner.IsDeleted = true;
            banner.ModifiedOn = DateTime.UtcNow;


            //var directoryPath = Server.MapPath("~/Banners");
            //var bannerImageUrlArray = banner.ImageUrl.Split('/');
            //var bannerImageName = bannerImageUrlArray[bannerImageUrlArray.Length - 1];
            //System.IO.File.Delete(directoryPath + "/" + bannerImageName);

            unitOfWork.Complete();
            return Json("Success");
        }

        [HttpPost]
        public JsonResult UpdateStatus(int id)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var banner = unitOfWork.Banner.Find(b => b.Id == id).FirstOrDefault();
            banner.IsActive = !banner.IsActive;
            banner.ModifiedOn = DateTime.UtcNow;
            unitOfWork.Complete();
            return Json("Success");
        }
    }
}