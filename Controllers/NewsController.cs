﻿using FlatworldCMS.Database;
using FlatworldCMS.Helpers;
using FlatworldCMS.Infrastructure;
using FlatworldCMS.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FlatworldCMS.Controllers
{
    [MvcApplication.EmailErrorAttribute]
    public class NewsController : Controller
    {
        UnitOfWork unitOfWork;
        int hrs, mins;
        string timeZoneCookie;

        public JsonResult GetCategories()
        {
            var categories = GetAllCategories().Select(ac => new { Id = ac.Id, Name = ac.Name }).ToList();
            return Json(categories, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveNews(int categoryId, string title, string description, int id, HttpPostedFileBase file)
        {
            try
            {
                int newsId = 0;
                News newsObj;
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                if (id == 0)
                {
                    newsObj = new News { CategoryId = categoryId, Title = title, Description=description, CreatedOn = DateTime.UtcNow, CreatedBy = AuthenticatedUser.GetLoggedInCustomerDetails().Id, IsActive = true };
                    unitOfWork.News.Add(newsObj);
                    Slack.PostMessage(EnumModule.News, EnumAction.created, title, User.Identity.Name);
                }
                else
                {
                    newsObj = unitOfWork.News.Find(a => a.Id == id).FirstOrDefault();
                    newsObj.CategoryId = categoryId;
                    newsObj.Description = description;
                    newsObj.Title = title;
                    newsObj.ModifiedOn = DateTime.UtcNow;
                    newsObj.ModifiedBy = AuthenticatedUser.GetLoggedInCustomerDetails().Id;
                    //Slack.PostMessage(EnumModule.News, EnumAction.updated, title, User.Identity.Name);
                }
                unitOfWork.Complete();
                return Json("success");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        [HandleError]
        public JsonResult SaveNewsCategory(string name, int categoryId = 0)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            if (unitOfWork.NewsCategory.Find(ac => ac.Name == name.Trim() && ac.Id != categoryId).Any())
                return Json("Category Exists");

            var category = new NewsCategory();

            if (categoryId > 0)
                category = unitOfWork.NewsCategory.Find(c => c.Id == categoryId).FirstOrDefault();
            category.Name = name;
            if (categoryId == 0)
            {
                category.CreatedOn = DateTime.UtcNow;
                unitOfWork.NewsCategory.Add(category);
            }
            unitOfWork.Complete();
            return Json("Success");
        }

        [HttpGet]
        public JsonResult GetNews()
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var categories = GetAllCategories().Select(ac => new { Id = ac.Id, Name = ac.Name }).ToList();
            var query = unitOfWork.News.Find(n => 1 == 1);
            if (AuthenticatedUser.GetLoggedInCustomerDetails().Role != "Administrator")
                query = query.Where(n => n.IsActive);

            GetTimezoneDiff();

            List<NewsModel> news = query.OrderByDescending(a => a.CreatedOn).Select(a => new NewsModel
            {
                Id = a.Id,
                Title = a.Title,
                Category = a.NewsCategory.Name,
                Description=a.Description,
                FullDescription=a.Description,
                CreatedBy = a.CreatedBy != null ? (a.User.FirstName + " " + a.User.LastName) : "",
                CategoryId = a.CategoryId,
                CreatedOn = a.CreatedOn.AddHours(hrs).AddMinutes(mins).ToString("dd MMM yyyy, hh:mm tt"),
                IsActive = a.IsActive
            }).ToList();
            return Json(new { categories = categories, news = news }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ChangeStatus(int id)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            News news = unitOfWork.News.Find(a => a.Id == id).FirstOrDefault();
            news.IsActive = !news.IsActive;
            news.ModifiedOn = DateTime.UtcNow;
            unitOfWork.Complete();
            return Json("success");
        }

        private List<NewsCategory> GetAllCategories()
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var categories = unitOfWork.NewsCategory.GetAll().OrderBy(ac => ac.Name).ToList();
            return categories;
        }

        public ActionResult UploadNewsPhoto(HttpPostedFileBase file)
        {
            try
            {
                if (Request.Files.Count >0)
                {
                    file = Request.Files[0];
                    string extension = System.IO.Path.GetExtension(file.FileName);
                    string ImgPath = "~/Images/News/" ;
                    string fname = DateTime.Now.ToString("yyyyddmmhhmmssss");
                    string path = System.IO.Path.Combine(Server.MapPath(ImgPath), fname+extension);
                    if (!Directory.Exists(Server.MapPath(ImgPath)))
                        Directory.CreateDirectory(Server.MapPath(ImgPath));
                    file.SaveAs(path);
                    return Json("../../Images/News/" + fname + extension);
                }
            }
            catch (Exception ex)
            {
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteUnsavedNewsImages(string[] images)
        {
            try
            {
                if (images != null)
                {
                    foreach (var item in images)
                    {
                        string image = Server.MapPath(item.Replace("../..", "~"));
                        if (System.IO.File.Exists(image))
                            System.IO.File.Delete(image);
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// To calculate Timezone difference
        /// </summary>
        public void GetTimezoneDiff()
        {
            if (Request.Cookies["CMS_UserTimezone"] != null)
            {
                timeZoneCookie = Request.Cookies["CMS_UserTimezone"].Value.ToString();
                hrs = Convert.ToInt32(timeZoneCookie.Substring(0, 3));
                mins = Convert.ToInt32(timeZoneCookie.Substring(3, 2));
            }
        }
    }
}