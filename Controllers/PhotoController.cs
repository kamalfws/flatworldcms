﻿using FlatworldCMS.Database;
using FlatworldCMS.Helpers;
using FlatworldCMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FlatworldCMS.Controllers
{
    [MvcApplication.EmailErrorAttribute]
    public class PhotoController : Controller
    {

        UnitOfWork unitOfWork;

        // GET: Photo
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetPhotoAlbums()
        {

            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var Compid = LoggedCompanyInfo.GetCurrentSingleton().CompanyId;
            var photoObjs = unitOfWork.Album.GetAll().Where(a=>a.IsVideo==false && a.CompanyID== Compid)
                 .Select(m => new
                 {
                     Name = m.AlbumName,
                     Path = m.AlbumPath,
                     Id=m.AlbumId,
                     date=m.UploadedDate
                  
                 }).OrderByDescending(m => m.date).ToList();

            return Json(photoObjs, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SavePhotoAlbum(HttpPostedFileBase file, string Name, HttpPostedFileBase photos, int? Id )
        {
            try
            {
                var compId= LoggedCompanyInfo.GetCurrentSingleton().CompanyId;
                string relativePath = string.Empty;
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                if (file != null)
                {
                    var directoryPath = Server.MapPath("~/Photo");
                    if (!Directory.Exists(directoryPath))
                        Directory.CreateDirectory(directoryPath);

                    string[] fileExtension = file.FileName.Split('.');

                    relativePath = "~/Photo" + DateTime.Now.Ticks + "." + fileExtension[fileExtension.Length-1];

                    file.SaveAs(Server.MapPath(relativePath));
                }

                Album albumObj = new Album();

               

                if (Id != null)
                {
                    albumObj = unitOfWork.Album.Find(e => e.AlbumId == Id).FirstOrDefault();

                    if (albumObj != null)
                    {
                        albumObj.AlbumName = Name;
                        albumObj.UploadedBy = 0;
                        albumObj.UploadedDate = DateTime.UtcNow;
                        albumObj.CompanyID = compId;
                        albumObj.IsPublished = true;
                        albumObj.IsVideo = false;

                        if (file != null)
                            albumObj.AlbumPath = relativePath.Replace("~", ConfigurationManager.AppSettings["ApplicationUrl"].ToString());
                     
                    }
                }
                else
                {

                    albumObj.AlbumName = Name;
                    albumObj.CompanyID = compId;
                    albumObj.UploadedBy = 0;
                    albumObj.UploadedDate = DateTime.UtcNow;
                    albumObj.IsPublished = true;
                    albumObj.IsVideo = false;

                    if (file != null)
                        albumObj.AlbumPath = relativePath.Replace("~", ConfigurationManager.AppSettings["ApplicationUrl"].ToString());
                     unitOfWork.Album.Add(albumObj);
                }

               

                Slack.PostMessage(EnumModule.Photo, EnumAction.created, Name, User.Identity.Name);

                unitOfWork.Complete();

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SavePhotos(HttpPostedFileBase[] files,int ID,string Name)
        {
            try
            {
                string relativePath = string.Empty;
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                var directoryPath = Server.MapPath("~/Photos");
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
             
                foreach (HttpPostedFileBase file in files)
                {
                    unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

                    string[] fileExtension = file.FileName.Split('.');

                    relativePath = "~/Photo/" + DateTime.Now.Ticks + "." + fileExtension[fileExtension.Length - 1];

                    file.SaveAs(Server.MapPath(relativePath));

                    PhotoNVideo photoObj = new PhotoNVideo();

                    photoObj.AlbumId = ID;

                    photoObj.Path = relativePath.Replace("~", ConfigurationManager.AppSettings["ApplicationUrl"].ToString()); ;
                    photoObj.Caption = "";
                    photoObj.IsVideo = false;
                    photoObj.UploadedOn = DateTime.UtcNow;

                    unitOfWork.PhotoNVideo.Add(photoObj);
                    unitOfWork.Complete();
                }
                Slack.PostMessage(EnumModule.Photo, EnumAction.updated, Name, User.Identity.Name);

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPhotos(int? ID)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var compId = LoggedCompanyInfo.GetCurrentSingleton().CompanyId;
            var companyid = unitOfWork.Album.GetAll().Where(a => a.AlbumId == ID).FirstOrDefault();
            var photoObjs = unitOfWork.PhotoNVideo.GetAll().Where(a => a.IsVideo == false&&a.AlbumId== ID&&companyid.CompanyID== compId).OrderByDescending(d=>d.UploadedOn)
                 .Select(m => new
                 {
                     url = m.Path,
                     thumbUrl = m.Path,
                     caption = m.Name,
                     date = m.UploadedOn,
                     Id=m.ID

                 }).ToList();

            return Json(photoObjs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeletePhoto(Int32 photoId)
        {
            try
            {
                var directoryPath = Server.MapPath("~/Photos");

                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

                var photo = unitOfWork.PhotoNVideo.GetAll().Where(d => d.ID == photoId).FirstOrDefault();

                if (photo != null)
                {
                    if (!string.IsNullOrEmpty(photo.Path))
                    {

                        DeletePhoto(photo.Path);
                    }
                        unitOfWork.PhotoNVideo.Remove(photo);
                }
             
                unitOfWork.Complete();

                return Json("Success");

            }
            catch (Exception ex)
            {
                return Json("Error");

            }
        }


        private void DeletePhoto(string fileName)
        {
            var directoryPath = Server.MapPath("~/Photo/");
            string[] paths = fileName.Split('/');
            string filename = paths[paths.Count() - 1];
            string path = fileName.Replace(ConfigurationManager.AppSettings["ApplicationUrl"].ToString(), "");

            path = Path.Combine(directoryPath, filename);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
        }

        [HttpPost]
        public JsonResult DeletePhotoAlbum(Int32 photoalbumId)
        {
            try
            {
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

                try
                {
                    var album = unitOfWork.PhotoNVideo.GetAll().Where(d => d.AlbumId == photoalbumId).Select(m => new { FilePath = m.Path, id = m.ID }).ToList();

                    if (album != null)
                    {
                        for (int i = 0; i < album.Count(); i++)
                        {
                            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

                            DeletePhoto(album[i].FilePath);

                             Int64 docID = album[i].id;
                           
                            PhotoNVideo doc = unitOfWork.PhotoNVideo.Find(f => f.ID == docID).FirstOrDefault();
                            unitOfWork.PhotoNVideo.Remove(doc);
                            unitOfWork.Complete();
                        }
                    }
                }
                catch (Exception  ex)
                { }
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                Album albumObj = unitOfWork.Album.Find(i => i.AlbumId == photoalbumId).FirstOrDefault();
                DeletePhoto(albumObj.AlbumPath);

                unitOfWork.Album.Remove(albumObj);
                unitOfWork.Complete();


                return Json("Success");

            }
            catch (Exception ex)
            {
                return Json("Error");

            }
        }
    }
}