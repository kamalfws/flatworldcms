﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using FlatworldCMS.Models;
using FlatworldCMS.Infrastructure;
using System.Web.Security;
using FlatworldCMS.Database;
using System.Configuration;
using FlatworldCMS.Helpers;

namespace FlatworldCMS.Controllers
{
    public class AccountController : Controller
    {

        UnitOfWork unitOfWork;

        //
        // GET: /Account/Login
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            //if (User.Identity.IsAuthenticated)
            //    return Redirect(ConfigurationManager.AppSettings["ApplicationUrl"] + "#/Dashboard");
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [HandleError]
        public JsonResult Login(string email, string returnUrl)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var userDetails = unitOfWork.User.Find(u => u.Email == email && (u.IsActiveinIntranet != null && u.IsActiveinIntranet.Value)).FirstOrDefault();
            if (userDetails != null)
            {
                CreateAuthentication(userDetails);
             return Json(Session["Plan"]!=null? Session["Plan"].ToString():"1");
            }
            else
            {
                return Json("Invalid Email");
            }

           
        }

        public JsonResult LogOut()
        {
            FormsAuthentication.SignOut();
            return Json("success", JsonRequestBehavior.AllowGet);
        }


        public JsonResult UserData()
        {
            //ErrorLog.Logger("Fetching data");
            var data = AuthenticatedUser.GetLoggedInCustomerDetails();
            //ErrorLog.Logger("Email: " + data.Email);
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        #region Helpers

        private FormsAuthenticationTicket CreateAuthentication(User userDetails)
        {
            var now = DateTime.UtcNow.ToLocalTime();
            var ticket = new FormsAuthenticationTicket(
            1 /*version*/, (userDetails.FirstName + " " + userDetails.LastName).Trim(),
                now,
                now.Add(FormsAuthentication.Timeout),
            false, "UserId:" + userDetails.UserId + ";Email:" + userDetails.Email + ";Role:" + userDetails.Role+";ProfilePath-"+userDetails.ProfilePicPath+";CompanyId:" + userDetails.Company,
                FormsAuthentication.FormsCookiePath);
            LoggedCompanyInfo loggedUserInfo = LoggedCompanyInfo.GetCurrentSingleton();
            if(userDetails.Role!="SuperUser")
            LoggedCompanyInfo.GetCurrentSingleton().CompanyId = (Int32)userDetails.Company;
            var encryptedTicket = FormsAuthentication.Encrypt(ticket);
            //Session["Company"] = userDetails.Company;
            //Session["Plan"] = unitOfWork.Company.Find(c => c.ID == userDetails.Company).FirstOrDefault().PaymentPlan;
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            cookie.HttpOnly = true;
            if (ticket.IsPersistent)
            {
                cookie.Expires = ticket.Expiration;
            }
            cookie.Secure = FormsAuthentication.RequireSSL;
            cookie.Path = FormsAuthentication.FormsCookiePath;
            if (FormsAuthentication.CookieDomain != null)
            {
                cookie.Domain = FormsAuthentication.CookieDomain;
            }
            //FormsAuthentication.SetAuthCookie(userDetails.FullName, false);
            Response.Cookies.Add(cookie);
            return ticket;
        }

        public ActionResult Register()
        {

            return View();
        }
             



        #endregion
    }
}