﻿using FlatworldCMS.Database;
using FlatworldCMS.Helpers;
using FlatworldCMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FlatworldCMS.Controllers
{
    [MvcApplication.EmailErrorAttribute]
    public class VideoController : Controller
    {
        UnitOfWork unitOfWork;
        int compId = LoggedCompanyInfo.GetCurrentSingleton().CompanyId;


        // GET: Vedio
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SaveVideo(HttpPostedFileBase file,string name,string url,int albumID)
        {
            try
            {
                string relativePath = string.Empty;
             
                var directoryPath = Server.MapPath("~/Video");
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
              
                if (file != null)
                {
                    string fileExtension = file.FileName.Split('.')[1];

                    relativePath = "~/Video/" + DateTime.Now.Ticks + "." + fileExtension;

                    file.SaveAs(Server.MapPath(relativePath));
                }
                else
                {
                    relativePath = url.Replace("watch?v=", "embed/");
                    string vimeoURL = "https://vimeo.com/";
                    if (url.Contains(vimeoURL))
                    {
                       
                        string Baseurl = "https://player.vimeo.com/video/";

                        relativePath =Baseurl+relativePath.Substring(relativePath.IndexOf(vimeoURL)+vimeoURL.Length) ;
                    }
                   
                }
            

                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

                PhotoNVideo photoObj = new PhotoNVideo();

                    photoObj.AlbumId = albumID;

                    photoObj.Path = relativePath.Replace("~", ConfigurationManager.AppSettings["ApplicationUrl"].ToString()); ;
                    photoObj.Name = name;
                    photoObj.IsVideo = true;
                    photoObj.UploadedOn = DateTime.UtcNow;
                photoObj.Caption = name;
                photoObj.UploadedBy = "0";
                    unitOfWork.PhotoNVideo.Add(photoObj);
                    unitOfWork.Complete();
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveVideoName(string Name, int Id)
        {
            try
            {
                string relativePath = string.Empty;
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

                PhotoNVideo albumObj = new PhotoNVideo();

               albumObj = unitOfWork.PhotoNVideo.Find(e => e.ID== Id).FirstOrDefault();
                var uploadedby = Convert.ToString(0);
                    if (albumObj != null)
                    {
                        albumObj.Name = Name;
                        albumObj.Caption = Name;
                        albumObj.UploadedBy = uploadedby;
                        albumObj.UploadedOn = DateTime.UtcNow;
                    }
                

                unitOfWork.Complete();
                Slack.PostMessage(EnumModule.Video, EnumAction.created, Name, User.Identity.Name);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveVideoAlbum(string Name, int? Id)
        {
            try
            {
                string relativePath = string.Empty;
                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
               
                Album albumObj = new Album();

                if (Id != null)
                {
                    albumObj = unitOfWork.Album.Find(e => e.AlbumId == Id).FirstOrDefault();

                    if (albumObj != null)
                    {
                        albumObj.AlbumName = Name;
                        albumObj.UploadedBy = 0;
                        albumObj.CompanyID = compId;
                        albumObj.UploadedDate = DateTime.UtcNow;
                        albumObj.IsPublished = true;
                        albumObj.AlbumPath = "";
                        albumObj.IsVideo = true;
                    }
                }
                else
                {
                    albumObj.AlbumName = Name;
                    albumObj.UploadedBy = 0;
                    albumObj.CompanyID = compId;
                    albumObj.UploadedDate = DateTime.UtcNow;
                    albumObj.IsPublished = true;
                    albumObj.IsVideo = true;
                    albumObj.AlbumPath = "";
                    unitOfWork.Album.Add(albumObj);
                }

                unitOfWork.Complete();
                Slack.PostMessage(EnumModule.Video, EnumAction.created, Name, User.Identity.Name);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetVideoAlbums()
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

            var photoObjs = unitOfWork.Album.GetAll().Where(a => a.IsVideo == true && a.CompanyID== compId).OrderByDescending(m => m.UploadedDate)
                 .Select(m => new
                 {
                     Name = m.AlbumName,
                     Path = m.AlbumPath,
                     Id = m.AlbumId,
                     date = m.UploadedDate

                 }).ToList();

            return Json(photoObjs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteVideo(Int32 videoId)
        {
            try
            {
                var directoryPath = Server.MapPath("~/Videos");

                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

                var video = unitOfWork.PhotoNVideo.GetAll().Where(d => d.ID == videoId).FirstOrDefault();

                if (video != null)
                {
                    if (!string.IsNullOrEmpty(video.Path))
                    {
                        DeleteVideo(video.Path);
                    }
                    unitOfWork.PhotoNVideo.Remove(video);
                }

                unitOfWork.Complete();

                return Json("Success");

            }
            catch (Exception ex)
            {
                return Json("Error");

            }
        }

        //[HttpPost]
        //public JsonResult EditVideo(Int32 videoId)
        //{
        //    try
        //    {
        //        var directoryPath = Server.MapPath("~/Videos");

        //        unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

        //        var video = unitOfWork.PhotoNVideo.GetAll().Where(d => d.ID == videoId).FirstOrDefault();

        //        if (video != null)
        //        {
        //            video.Name=
        //        }

        //        unitOfWork.Complete();

        //        return Json("Success");

        //    }
        //    catch (Exception ex)
        //    {
        //        return Json("Error");

        //    }
        //}

        [HttpPost]
        public JsonResult DeleteVideoAlbum(Int32 videoalbumId)
        {
            try
            {
                var directoryPath = Server.MapPath("~/Videos");
                try
                {
                    unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                    var album = unitOfWork.PhotoNVideo.GetAll().Where(d => d.AlbumId == videoalbumId).Select(m => new { FilePath = m.Path, id = m.ID }).ToList();

                    if (album != null)
                    {
                        for (int i = 0; i < album.Count(); i++)
                        {
                            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                            DeleteVideo(album[i].FilePath);

                            Int64 docID = album[i].id;
                         
                            PhotoNVideo doc = unitOfWork.PhotoNVideo.Find(f => f.ID == docID).FirstOrDefault();
                            unitOfWork.PhotoNVideo.Remove(doc);
                            unitOfWork.Complete();
                        }
                    }
                }
                catch (Exception ex)
                { }

                unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
                Album albumObj = unitOfWork.Album.Find(i => i.AlbumId == videoalbumId).FirstOrDefault();


                unitOfWork.Album.Remove(albumObj);
                unitOfWork.Complete();


                return Json("Success");

            }
            catch (Exception ex)
            {
                return Json("Error");

            }
        }

        public JsonResult GetVideos(int ID)
        {
            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());
            var companyid = unitOfWork.Album.GetAll().Where(a => a.AlbumId == ID).FirstOrDefault();
            var photoObjs = unitOfWork.PhotoNVideo.GetAll().Where(a => a.IsVideo == true&&a.AlbumId==ID && companyid.CompanyID==compId).OrderByDescending(a=>a.UploadedOn)
                 .Select(m => new
                 {
                     url = m.Path,
                     thumbUrl = "",
                     caption = m.Name,
                     date = m.UploadedOn,
                     Id = m.ID,
                     type= "video"

                 }).ToList();

            return Json(photoObjs, JsonRequestBehavior.AllowGet);
        }

        private void DeleteVideo(string fileName)
        {
            var directoryPath = Server.MapPath("~/Video/");
            string[] paths = fileName.Split('/');
            string filename = paths[paths.Count() - 1];
            string path = fileName.Replace(ConfigurationManager.AppSettings["ApplicationUrl"].ToString(), "");

            path = Path.Combine(directoryPath, filename);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
        }
    }
}