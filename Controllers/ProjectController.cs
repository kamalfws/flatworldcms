﻿using Common.Helpers;
using FlatworldCMS.Database;
using FlatworldCMS.Helpers;
using FlatworldCMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FlatworldCMS.Controllers
{
    [MvcApplication.EmailErrorAttribute]
    public class ProjectController : Controller
    {
        // GET: Project
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetProjects(GridSearchParameters userSearch)
        {
            try
            {
                FlatworldCMSEntities context = new FlatworldCMSEntities();

                List<Projects_GetList_Result> projects = new List<Projects_GetList_Result>();

                projects = context.Projects_GetList(userSearch.filterBy, LoggedCompanyInfo.GetCurrentSingleton().CompanyId, Convert.ToInt32(userSearch.currentPage), Convert.ToInt32(userSearch.pageItems), userSearch.orderBy, userSearch.orderByReverse).ToList();

                //if (userSearch.filterByFields == "User")
                //    projects = projects.Where(a => a.Active == true).ToList();
                return Json(projects, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EnableDisableProject(string projectID, bool enable)
        {
            UnitOfWork unitOfWork;

            unitOfWork = new UnitOfWork(new Database.FlatworldCMSEntities());

            Project dbProject = unitOfWork.Project.Find(m => m.ZohoProjectID == projectID).FirstOrDefault();

            dbProject.Active = enable;

            unitOfWork.Complete();

            return Json("Success", JsonRequestBehavior.AllowGet);

        }
    }
}