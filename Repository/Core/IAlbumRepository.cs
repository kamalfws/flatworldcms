﻿using FlatworldCMS.Core;
using FlatworldCMS.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlatworldCMS.Repository.Core
{
    public interface IAlbumRepository : IGenericRepository<Album>
    {
    }
}
