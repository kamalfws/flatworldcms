﻿using FlatworldCMS.Core;
using FlatworldCMS.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlatworldCMS.Repository.Core
{
    public interface IPlanRepository : IGenericRepository<Plan>
    {
    }
}