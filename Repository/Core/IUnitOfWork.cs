﻿namespace FlatworldCMS.Core
{
    interface IUnitOfWork
    {
        void Complete();
    }
}