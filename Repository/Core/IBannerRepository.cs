﻿using FlatworldCMS.Core;
using FlatworldCMS.Database;

namespace FlatworldCMS.Repository.Core
{
    public interface IBannerRepository : IGenericRepository<Banner>
    {
    }
}
