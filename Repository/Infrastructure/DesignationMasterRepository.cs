﻿using FlatworldCMS.Database;
using FlatworldCMS.Infrastructure;
using FlatworldCMS.Repository.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FlatworldCMS.Repository.Infrastructure
{
    public class DesignationMasterRepository : GenericRepository<DesignationMaster>, IDesignationMasterRepository
    {
        public DesignationMasterRepository(DbContext context) : base(context)
        {

        }
    }
}