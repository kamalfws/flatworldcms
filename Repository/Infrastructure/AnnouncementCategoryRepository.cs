﻿using FlatworldCMS.Infrastructure;
using FlatworldCMS.Repository.Core;
using FlatworldCMS.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace FlatworldCMS.Repository.Infrastructure
{
    public class AnnouncementCategoryRepository : GenericRepository<AnnouncementCategory>, IAnnouncementCategoryRepository
    {
        public AnnouncementCategoryRepository(DbContext context) : base(context)
        {

        }
    }
}