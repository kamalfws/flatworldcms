﻿using FlatworldCMS.Database;
using FlatworldCMS.Infrastructure;
using FlatworldCMS.Repository.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FlatworldCMS.Repository.Infrastructure
{
    public class EventRepository : GenericRepository<Event>, IEventRepository
    {

        public EventRepository(DbContext context) : base(context)
        {

        }
    }
}