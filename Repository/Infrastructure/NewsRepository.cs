﻿using FlatworldCMS.Database;
using FlatworldCMS.Infrastructure;
using FlatworldCMS.Repository.Core;
using System.Data.Entity;

namespace FlatworldCMS.Repository.Infrastructure
{
    public class NewsRepository : GenericRepository<News>, INewsRepository
    {
        public NewsRepository(DbContext context) : base(context)
        {

        }
    }
}