﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlatworldCMS.Core;
using FlatworldCMS.Database;
using FlatworldCMS.Repository.Core;
using FlatworldCMS.Infrastructure;
using System.Data.Entity;

namespace FlatworldCMS.Repository.Infrastructure
{

    public class ProjectRepository : GenericRepository<Project>, IProjectRepository
    {
        public ProjectRepository(DbContext context) : base(context)
        {
        }

    }

  
}