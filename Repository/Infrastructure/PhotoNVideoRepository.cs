﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlatworldCMS.Database;
using FlatworldCMS.Infrastructure;
using FlatworldCMS.Repository.Core;
using System.Data.Entity;

namespace FlatworldCMS.Repository.Infrastructure
{
    public class PhotoNVideoRepository :GenericRepository<PhotoNVideo>, IPhotoNVideoRepository

    {
        public PhotoNVideoRepository(DbContext context) : base(context)
        {

    }
}
}