﻿using FlatworldCMS.Database;
using FlatworldCMS.Infrastructure;
using FlatworldCMS.Repository.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FlatworldCMS.Repository.Infrastructure
{
    public class RoleMasterRepository : GenericRepository<RoleMaster>, IRoleMasterRepository
    {
        public RoleMasterRepository(DbContext context) : base(context)
        {

        }
    }
}