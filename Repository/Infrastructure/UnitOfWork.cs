﻿using FlatworldCMS.Core;
using FlatworldCMS.Database;
using FlatworldCMS.Repository.Core;
using FlatworldCMS.Repository.Infrastructure;
using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;

namespace FlatworldCMS.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FlatworldCMSEntities _dbContext;
        private readonly ObjectContext _objectContext;
        private readonly IDbTransaction _transaction;

        public UnitOfWork(FlatworldCMSEntities context)
        {
            _dbContext = context;
            _objectContext = ((IObjectContextAdapter)_dbContext).ObjectContext;
            if (_objectContext.Connection.State != ConnectionState.Open)
            {
                try
                {
                    _objectContext.Connection.Open();
                    _transaction = _objectContext.Connection.BeginTransaction();
                }
                catch (Exception ex)
                {
                    _objectContext.Connection.Close();
                    //Helpers.Logger("Exception: " + ex.Message + " - Inner Exception: " + ex.StackTrace + " - Stack Trace: " + ex.StackTrace);
                }
            }
        }


        public IUserRepository User
        {
            get
            {
                return new UserRepository(_dbContext);
            }
        }

        public IRoleMasterRepository RoleMaster
        {
            get
            {
                return new RoleMasterRepository(_dbContext);
            }
        }
        public IDesignationMasterRepository DesignationMaster
        {
            get
            {
                return new DesignationMasterRepository(_dbContext);
            }
        }
        public IDepartmentMasterRepository DepartmentMaster
        {
            get
            {
                return new DepartmentMasterRepository(_dbContext);
            }
        }
        public IAlbumRepository Album
        {
            get
            {
                return new AlbumRepository(_dbContext);
            }
        }
        public IDocumentRepository Document
        {
            get { return new DocumentRepository(_dbContext);
            }
        }

        public IDocumentCategoryRepository DocumentCategory
        {
            get
            {
                return new DocumentCategoryRepository(_dbContext);
            }
        }

        public IEventRepository Events
        {
            get
            {
                return new EventRepository(_dbContext);
            }
        }

        public IAnnouncementRepository Announcement
        {
            get
            {
                return new AnnouncementRepository(_dbContext);
            }
        }

        public IAnnouncementCategoryRepository AnnouncementCategory
        {
            get
            {
                return new AnnouncementCategoryRepository(_dbContext);
            }
        }

        public INewsCategoryRepository NewsCategory
        {
            get
            {
                return new NewsCategoryRepository(_dbContext);
            }
        }

        public INewsRepository News
        {
            get
            {
                return new NewsRepository(_dbContext);
            }
        }

        public IBannerRepository Banner
        {
            get
            {
                return new BannerRepository(_dbContext);
            }
        }

        public IPhotoNVideoRepository  PhotoNVideo
        {
            get
            {
                return new PhotoNVideoRepository(_dbContext);
            }
        }


        public IProjectRepository Project
        {
            get
            {
                return new ProjectRepository(_dbContext);
            }
        }

        public IPlanRepository Plan
        {
            get
            {
                return new PlanRepository(_dbContext);
            }
        }

        #region "Operations on repository"

        /// <summary>
        /// Save pending changes to database
        /// </summary>
        public void Complete()
        {
            try
            {
                _dbContext.SaveChanges();
                _transaction.Commit();
            }
            catch (Exception ex)
            {
                Rollback();
                throw;
            }
            finally
            {
                Dispose();
            }
        }

        private void Rollback()
        {
            this._transaction.Rollback();

            foreach (var entry in _dbContext.ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        break;
                }
            }
        }

        /// <summary>
        /// Dispose current DBContext connection
        /// </summary>
        public void Dispose()
        {
            if (_objectContext != null)
            {
                if (_objectContext.Connection.State == ConnectionState.Open)
                {
                    _objectContext.Connection.Close();
                }
            }
        }
        #endregion
    }
}