﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace FlatworldCMS.Models
{
    public static class AuthenticatedUser
    {
        public static LoggedInUser GetLoggedInCustomerDetails(FormsAuthenticationTicket ticket = null)
        {
            try
            {
                if (ticket == null)
                {
                    FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;
                    ticket = id.Ticket;
                }

                var userData = ticket.UserData;
                var splittedData = userData.Split(';');
                var loggedInUserData = new LoggedInUser
                {
                    Id = Convert.ToInt32(splittedData[0].Split(':')[1]),
                    Email = splittedData[1].Split(':')[1],
                    Role = splittedData[2].Split(':')[1],
                    profilepicPath= splittedData[3].Split('-')[1]
                };
                loggedInUserData.FullName = ticket.Name;
                return loggedInUserData;
            }
            catch (Exception ex)
            {
                ErrorLog.Logger(ex);
                return null;
            }
        }
    }
    public class LoggedInUser
    {
        public int Id { get; set; }
        public string Email { get; set; }

        public string Role { get; set; }

        public string FullName { get; set; }
        public string profilepicPath { get; set; }
    }


}