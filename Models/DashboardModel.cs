﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlatworldCMS.Models
{

    public class DashboardModel
    {
        public List<AnnouncememtModel> Announcements { get; set; }
        public List<ProjectModel> Projects { get; set; }
        public List<BirthdayModel> Birthdays { get; set; }
        public List<NewsModel> News { get; set; }
        public List<EventModel> Events { get; set; }
        public List<DocumentModel> Documents { get; set; }

        public List<VideoModel> Videos { get; set; }
        public List<PhotoModel> Photos { get; set; }

        public List<BannerModel> Banners { get; set; }


    }
    public class AnnouncememtModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string BriefDescription { get; set; }
        public string Description { get; set; }
        public string AnnouncedBy { get; set; }
        public string AnnouncedOn { get; set; }
        public string Category { get; set; }

        public int CategoryId { get; set; }
    }

    public class ProjectModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Owner { get; set; }
        public string Description { get; set; }
    }

    public class BirthdayModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BirthDate { get; set; }
        public string ProfilePicPath { get; set; }
        public string Designation { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }

    public class NewsModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FullDescription { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

    }

    public class EventModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string EventDate { get; set; }
        public string Image { get; set; }

    }
    public class DocumentModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Count { get; set; }
    }

    public class VideoModel
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
    public class PhotoModel
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }


    public class BannerModel
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }

}