﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace FlatworldCMS.Models
{
    public class RoleAuthorizeAttribute : AuthorizeAttribute
    {
        private bool isValidRole;
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            isValidRole = true;
            bool authorize = false;
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return authorize;

            FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;
            FormsAuthenticationTicket ticket = id.Ticket;

            var userData = ticket.UserData;
            var splittedData = userData.Split(';');
            var roles = splittedData[3].Split(':')[1].Split(',');

            authorize = roles.Any(r => this.Roles.Split(',').Contains(r)); // checking active users with allowed roles.  
            isValidRole = authorize;

            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectResult(ConfigurationManager.AppSettings["ApplicationUrl"] + "Account/Login");
        }
    }
}