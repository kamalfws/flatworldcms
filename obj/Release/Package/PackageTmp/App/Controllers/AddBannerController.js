﻿
CMSapp.controller('AddBannerCtrl', function ($scope, Upload, $mdDialog, $mdSidenav, modalService) {

    $scope.SaveBanner = function () {
        if ($scope.bannerForm.bannerFile.$valid && $scope.file) {
            $scope.showRequiredError = false;
            if ($scope.bannerForm.$valid)
                $scope.upload($scope.file);
        }
        else {
            $scope.showRequiredError = true;
        }
    }
    // upload on file select or drop
    $scope.upload = function (file) {
        Upload.upload({
            url: getBaseUrl() + '/Banners/SaveBanner',
            data: { 'file': file, 'title': $scope.bannerTitle }
        }).then(function (resp) {
            $scope.Answer("Success");
        }, function (resp) {
            $scope.Answer("Failure");
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        });
    };

    $scope.Cancel = function () {
        $mdDialog.cancel();
    };

    $scope.Answer = function (status) {
        $mdDialog.hide("success");
    }
});


