﻿
CMSapp.controller('AddEventCtrl', function ($scope, Upload, $mdDialog, $mdSidenav, modalService) {
  
    
    $scope.SaveEvent = function () {
        if ($scope.form.file.$valid && $scope.file) {
            $scope.upload($scope.file);
        }
        else
            modalService.show("Please Upload image Files", StatusType.ERROR);
    }
    // upload on file select or drop
    $scope.upload = function (file) {
        Upload.upload({
            url: getBaseUrl() + '/Events/SaveEvent',
            data: { 'file': file, 'eventName': $scope.Name, 'location': $scope.Location, 'Description': $scope.Description, 'startDate': $scope.StartDate,'Id':$scope.EventId }
        }).then(function (resp) {
            $scope.Answer("Success");
        }, function (resp) {
            $scope.Answer("Failure");
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        });
    };

    $scope.Cancel = function () {
        $mdDialog.cancel();
    };

    $scope.Answer = function (status) {
        $mdDialog.hide("success");
    }
});


