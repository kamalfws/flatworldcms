﻿using System.Web;
using System.Web.Optimization;

namespace FlatworldCMS
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.10.2.js",
                        "~/Scripts/moment.js",
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-animate.js",
                        "~/Scripts/angular-aria.js",
                        "~/Scripts/angular-messages.js",
                        "~/Scripts/angular-spinner/spin.min.js",
                        "~/Scripts/angular-spinner/angular-loading-spinner.js",
                        "~/Scripts/angular-spinner/angular-spinner.min.js",
                        "~/Scripts/angular-material/angular-material.min.js",
                        "~/Scripts/angular-translate.js",
                        "~/Scripts/angular-cookies.js",
                        "~/Scripts/angular-local-storage.js",
                        "~/Scripts/angular-translate-storage-cookie.js",
                        "~/Scripts/angular-translate-storage-local.js",
                        "~/Scripts/angular-translate-handler-log.js",
                        "~/Scripts/angular-translate-loader-static-files.js",
                        "~/Scripts/angular-route.js",
                        "~/Scripts/angular-sanitize.min.js",
                        "~/Scripts/livestamp.min.js",
                        "~/Scripts/angular-livestamp.js",
                        "~/Scripts/material.min.js",
                        "~/Scripts/chartist.min.js",
                        "~/Scripts/bootstrap-notify.js",
                        "~/Scripts/material-dashboard.js",
                        "~/Scripts/trNgGrid/trNgGrid.js",
                        "~/Scripts/angular-resource.min.js",
                        "~/Scripts/ngStorage.min.js",
                        "~/Scripts/tinymce/tinymce.min.js"
                       // "~/Scripts/ng-image-compress.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/ng-videosharing-embed.js",
                      "~/Scripts/angular-bootstrap-lightbox.js",
                      "~/Scripts/ui-bootstrap.js",
                      "~/Scripts/ng-file-upload-shim.min.js",
                      "~/Scripts/ng-file-upload.min.js",

                      //>>>Add your angular app custon js files
                      "~/App/FlatworldCMSApp.js",
                      "~/App/Controllers/VideoAlbumsController.js",
                      "~/App/Controllers/VideoGalleryController.js",
                      "~/App/Controllers/PhotoAlbumsController.js",
                      "~/App/Controllers/UserController.js",
                      "~/App/Controllers/DashboardController.js",
                      "~/App/Controllers/BannersController.js",
                      "~/App/Controllers/AddBannerController.js",
                      "~/App/Controllers/BirthdaysController.js",
                      "~/App/Services/modalService.js",
                      "~/App/Controllers/ProjectController.js",
                      "~/App/Controllers/DocumentController.js",
                      "~/App/Controllers/AddDocumentController.js",
                      "~/App/Controllers/DocumentListController.js",
                      "~/App/Controllers/AddMultipleDocumentController.js",
                      "~/App/Controllers/AnnouncementsController.js",
                      "~/App/Controllers/AddAnnouncementController.js",
                      "~/App/Controllers/NewsController.js",
                      "~/App/Controllers/AddNewsController.js",
                      "~/App/Controllers/EventsController.js",
                      "~/App/Controllers/AddEventController.js",
                      "~/App/Controllers/EditEventController.js",
                      "~/App/Controllers/EventReadMoreController.js",
                      "~/App/Services/LoginService.js",
                      "~/App/Controllers/AddPhotoAlbum.js",
                      "~/App/Controllers/PhotoGalleryController.js",
                      "~/App/Controllers/AddPhotosController.js",
                      "~/App/Controllers/ProfileController.js",
                      "~/App/Controllers/AnnouncementReadMoreController.js",
                      "~/App/Controllers/ReadMoreProjectController.js",
                      "~/App/Controllers/ReadMoreBirthdayController.js",
                       "~/App/Controllers/PaymentController.js",
                       "~/App/Controllers/Register.js",
                        "~/App/Controllers/ApplicationSettingsController.js"
                      ));



            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/angular-material.css",
                      "~/Content/bootstrap.min.css",

                      //"~/Content/angular-bootstrap-lightbox.css",
                      "~/Content/material-dashboard.css"
                      ));

            //BundleTable.EnableOptimizations = true;
        }
    }
}
