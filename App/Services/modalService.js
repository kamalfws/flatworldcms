﻿CMSapp.service("modalService", function ($mdToast) {
    this.show = function (_message, StatusType) {
        var strType = "success";
        if (StatusType == 0)
            strType = "success";
        else if (StatusType == 1)
            strType = "error";
        else if (StatusType == 2)
            strType = "warning";
        $mdToast.show($mdToast.simple().textContent(_message).theme(strType + "-toast").position("top right").hideDelay(3000));
    }
});