﻿angular.module('AuthServices', ['ngResource', 'ngStorage'])
.factory('Auth', function ($resource, $rootScope, $sessionStorage, $q, $http) {

    /**
     *  User profile resource
     */
    var Profile = $resource('/Account/UserData', {}, {
        UserData: {
            method: "GET",
            isArray: false
        }
    });

    var auth = {};

    /**
     *  Saves the current user in the root scope
     *  Call this in the app run() method
     */


    auth.login = function () {
        if (!auth.isLoggedIn()) {
            var get = $http({
                async: false,
                method: "GET",
                url: "/Account/UserData",
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {

                $sessionStorage.user = data;
                $rootScope.loggedInUser = $sessionStorage.user;
            });

            get.error(function (data, status) {
            });
        }
    };

    //auth.login = function () {
    //    debugger;
    //    if (!auth.isLoggedIn()) {
    //        return $q(function (resolve, reject) {
    //            Profile.UserData().$promise
    //            .then(function (data) {
    //                debugger;
    //                $sessionStorage.user = data;
    //                $rootScope.loggedInUser = $sessionStorage.user;
    //                resolve();
    //            }, function () {
    //                debugger;
    //                reject();
    //            });
    //        });
    //    }
    //};


    auth.logout = function () {
        delete $sessionStorage.user;
        delete $rootScope.loggedInUser;
    };


    auth.checkPermissionForView = function (view) {
        if (!view.requiresAuthentication) {
            return true;
        }

        return userHasPermissionForView(view);
    };


    var userHasPermissionForView = function (view) {
        if (!auth.isLoggedIn()) {
            return false;
        }

        if (!view.permission || !view.permission.length) {
            return true;
        }

        return auth.userHasPermission(view.permission);
    };


    auth.userHasPermission = function (permission) {
        if (!auth.isLoggedIn()) {
            return false;
        }

        var found = false;
        if ($sessionStorage.user.Role == permission || (permission == undefined && $sessionStorage.user.Role == "Administrator"))
            found = true;

        return found;
    };

    auth.userIsSuperUser = function (permission) {
        if (!auth.isLoggedIn()) {
            return false;
        }

        var found = false;
        if ($sessionStorage.user.Role == permission || (permission == undefined && $sessionStorage.user.Role == "SuperUser"))
            found = true;

        return found;
    };

    auth.currentUser = function () {
        return $sessionStorage.user;
    };


    auth.isLoggedIn = function () {
        return $sessionStorage.user != null;
    };

    auth.init = function () {
        if (auth.isLoggedIn()) {
            $rootScope.loggedInUser = auth.currentUser();
        }
    };
    return auth;
});