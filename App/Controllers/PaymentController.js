﻿CMSapp.controller('PaymentCtrl', function ($scope, $mdDialog, $http, modalService) {
    var Yearlydata;
    $scope.GetAllPlans = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/Payment/GetPlans",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });
        get.success(function (data, status) {
            $scope.plans = data;
            Yearlydata = data;
        });
        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    };

    $scope.Monthly = "Monthly";
    $scope.Yearly = "Yearly";
   
    $scope.GetAllPlans();

    $scope.Upgrade = function (plan) {
        if (plan.ID==1)
        {
            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/Payment/UpdatePaymentPlan",
                dataType: 'json',
             
                headers: { "Content-Type": "application/json" }
            });
            get.success(function (data, status) {
                $scope.plans = data;
                Yearlydata = data;
            });
            get.error(function (data, status) {
                modalService.show("Error Occured", StatusType.ERROR);
            });
        }

    }

    $scope.appliedClass = function (planType) {
       
        if (planType === "Monthly") {
            return "btn btn-blue";
        } else {
            return "btn btn-blue active"; // Or even "", which won't add any additional classes to the element
        }
    }

    $scope.PlanType = function (planType) {
        if ($scope.PlanPeriod != planType)
        {
            if (planType === "Monthly") {
                $scope.PlanPeriod = "Monthly";
                for (i = 0; i < Yearlydata.length; i++) {
                    if (Yearlydata[i].Amount > 0)
                        Yearlydata[i].Amount = Yearlydata[i].Amount / 12;
                }
                return "btn btn-blue active";
            }
            else {
                $scope.PlanPeriod = "Yearly";
                for (i = 0; i < Yearlydata.length; i++) {
                    if (Yearlydata[i].Amount > 0)
                        Yearlydata[i].Amount = Yearlydata[i].Amount * 12;
                }


                return "btn btn-blue"; // Or even "", which won't add any additional classes to the element
            }
            $scope.plans = Yearlydata;

            $scope.appliedClass = function (planType) {
                debugger;
                if (planType === "Monthly") {
                    return "btn btn-blue active";
                } else {
                    return "btn btn-blue"; // Or even "", which won't add any additional classes to the element
                }
            }
        }
    }
});