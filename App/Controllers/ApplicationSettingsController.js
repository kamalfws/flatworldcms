﻿CMSapp.controller('ApplicationSettingsController', function ($http, $scope, $location, modalService, $mdSidenav, $mdDialog, $mdToast) {
    $scope.filterBy = '';
    $scope.PopupButtonText = 'Save';
    $scope.pageSizeList = getPageSizeList();
    $scope.pPageSizeObj = PageSizeSelectedObj;
    $scope.PageSize = $scope.pPageSizeObj.selected;
    $scope.CompanyID = 0;
    $scope.searchBy = '';
    
    //Called from on-data-required directive.
    $scope.onServerSideItemsRequested = function (currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        LoadSettingsData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse);
    }

    // Called from global search box
    $scope.onChange = function () {
        LoadSettingsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Called from page size drop down changed.
    $scope.changePageSize = function () {
        $scope.PageSize = $scope.pPageSizeObj.selected;
    }

    $scope.clearSearchParams = function () {
        $scope.filterBy = '';
        LoadSettingsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    
    // Ajax call  to fetch data from server...
    function LoadSettingsData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {

        var SettingSearch = {
            currentPage: currentPage,
            pageItems: pageItems,
            filterBy: filterBy,
            filterByFields: angular.toJson(filterByFields),
            orderBy: orderBy,
            orderByReverse: orderByReverse
        }
        pCurrentPage = currentPage;
        pPageItems = pageItems;
        pOrderBy = orderBy;
        pOrderByReverse = orderByReverse
        var req = {
            method: 'POST',
            url: getBaseUrl() + '/Settings/GetAllSettings?nocache=' + new Date().getTime(),
            contentType: "application/json",
            data: JSON.stringify(SettingSearch)
        }
        $http(req).success(function (data) {
            $scope.Settings = data[0].ListOfItems;
            $scope.TotalRecordsCount = data[0].TotalRecordCount;
            $scope.Companies = data[1];
           
        }).error(function (error) {
            modalService.show('Error While loading Data.', StatusType.ERROR);
        });
    }

    //Clear all User Input fields
    $scope.clearUserFields = function () {

        $scope.HostName = $scope.UserName = $scope.Password = $scope.EnableSSL = $scope.PortNo = $scope.SlackIntegration = $scope.ZohoProjects = '';
        $scope.CompanyID = 'Select';
        $scope.SettingsInfodiv.$setPristine();
        $scope.SettingsInfodiv.$setUntouched();
    }

    $scope.ShowPopup = function (grd, add) {
        $scope.clearUserFields();
        $scope.Id = 0;
        if (add) {
            $scope.clearUserFields();
            //$scope.CompanyID = 'Select';
            $scope.CompanyID = 0;
        }
        else {
            $scope.CompanyID = grd.CompanyID;
            $scope.HostName = grd.HostName;
            $scope.UserName = grd.UserName;
            $scope.Password = grd.Password;
            $scope.EnableSSL = (grd.EnableSSL == 'Yes') ? '1' : '0';
            $scope.PortNo = grd.PortNo;
            $scope.SlackIntegration = (grd.SlackIntegration == 'Yes') ? '1' : '0';
            $scope.ZohoProjects = (grd.ZohoProjects == 'Yes') ? '1' : '0';
            }
        //$scope.PopupButtonText = add ? 'Save' : 'Update';
        //$mdSidenav('SettingsInfodiv').toggle();
        $mdDialog.show({
            contentElement: '#applicationSettingsDialog',
            parent: angular.element(document.body),
            //scope: $scope,
            //preserveScope: true
            
        });

        $scope.cid=2;
    }

    $scope.SaveOrUpdateSettingsInfo = function () {
        if ($scope.SettingsInfodiv.$valid) {
            var settingsData = {
                CompanyID: $scope.CompanyID,
                HostName: $scope.HostName,
                UserName: $scope.UserName,
                Password: $scope.Password,
                EnableSSL: ($scope.EnableSSL=='1')?true:false,
                PortNo: $scope.PortNo,
                SlackIntegration: ($scope.SlackIntegration=='1')?true:false,
                ZohoProjects: ($scope.ZohoProjects=='1')?true:false,
              }

            var req = {
                method: 'POST',
                url: getBaseUrl() + '/Settings/CrudOperation?nocache=' + new Date().getTime(),
                contentType: "application/json",
                data: JSON.stringify(settingsData)
            }
            $http(req).success(function (data) {
                if (data != undefined && data == 'True') {
                  modalService.show('Success', StatusType.SUCCESS);
                    //$mdSidenav('SettingsInfodiv').toggle();
                  $mdDialog.cancel();
                    $scope.clearUserFields();
                    LoadSettingsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
                }
                else {
                    modalService.show('Failure', StatusType.ERROR);
                    //$scope.Answer("Failure");
                }
            }).error(function (error) {
                modalService.show('Failure', StatusType.ERROR);
                //$scope.Answer("Failure");
            });
        }
    }

    $scope.cancel = function () {
        $mdDialog.cancel();
    };


});








//CMSapp.controller('AddBannerCtrl', function ($scope, Upload, $mdDialog, $mdSidenav, modalService) {

//    $scope.SaveBanner = function () {
//        if ($scope.bannerForm.bannerFile.$valid && $scope.file) {
//            $scope.showRequiredError = false;
//            if ($scope.bannerForm.$valid)
//                $scope.upload($scope.file);
//        }
//        else {
//            $scope.showRequiredError = true;
//        }
//    }
//    // upload on file select or drop
//    $scope.upload = function (file) {
//        Upload.upload({
//            url: getBaseUrl() + '/Banners/SaveBanner',
//            data: { 'file': file, 'title': $scope.bannerTitle }
//        }).then(function (resp) {
//            $scope.Answer("Success");
//        }, function (resp) {
//            $scope.Answer("Failure");
//        }, function (evt) {
//            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
//        });
//    };

//    $scope.Cancel = function () {
//        $mdDialog.cancel();
//    };

//    $scope.Answer = function (status) {
//        $mdDialog.hide("success");
//    }
//});


