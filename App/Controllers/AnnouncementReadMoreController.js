﻿CMSapp.controller('ReadMoreAnnouncementCtrl', function ($scope, $mdDialog, items) {
    $scope.Title = items.Title;
    $scope.Category = items.Category;
    $scope.Description = items.Description;
    $scope.AnnouncedOn = new Date(moment(items.AnnouncedOn).format("DD-MMM-YYYY"));
    $scope.AnnouncedBy = items.AnnouncedBy
   
    $scope.Cancel = function () {
        $mdDialog.cancel();
    };

    $scope.Answer = function (status) {
        $mdDialog.hide("success");
    }
});