﻿CMSapp.controller('ReadMoreProjectCtrl', function ($scope, $mdDialog, items) {

    $scope.Name = items.Name;
    $scope.Owner = items.Owner;
    $scope.Description = items.Description;

    $scope.Cancel = function () {
        $mdDialog.cancel();
    };

    $scope.Answer = function (status) {
        $mdDialog.hide("success");
    }
});