﻿CMSapp.controller('BannerCtrl', function ($scope, $mdDialog, $http, modalService) {
    $scope.GetAllBanners = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/Banners/GetBanners",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });
        get.success(function (data, status) {
            $scope.images = data;
        });
        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    };

    $scope.ShowAddBanner = function () {
        $mdDialog.show({
            controller: 'AddBannerCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/AddBanner.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            // targetEvent: ev,
             clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function (answer) {
            if (answer.toLowerCase() == "success") {
                modalService.show("Successfully Uploaded", StatusType.SUCCESS);
                $scope.GetAllBanners();
            }
            else
                modalService.show("Error Occured", StatusType.ERROR);
            //$scope.status = 'You said the information was "' + answer + '".';
        }, function () {

        });
    };

    $scope.DeleteBanner = function (id) {
        var get = $http({
            method: "POST",
            url: getBaseUrl() + "/Banners/DeleteBanner",
            data: { id: id },
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });
        get.success(function (data, status) {
            modalService.show("Banner Removed", StatusType.SUCCESS);
            $scope.GetAllBanners();
        });
        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    };


    $scope.ChangeStatus = function (id) {
        var confirm = $mdDialog.confirm()
      .title('Please confirm to change the status')
      .ariaLabel('Confirm')
      .ok('Yes')
      .cancel('No');
        $mdDialog.show(confirm).then(function () {
            $scope.UpdateStatus(id);
        }, function () {
        });
    }

    $scope.UpdateStatus = function (id) {
        var get = $http({
            method: "POST",
            url: getBaseUrl() + "/Banners/UpdateStatus",
            data: { id: id },
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });
        get.success(function (data, status) {
            modalService.show("Status Modified", StatusType.SUCCESS);
            $scope.GetAllBanners();
        });
        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    };

    $scope.GetAllBanners();

});


