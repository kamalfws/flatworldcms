﻿CMSapp.controller('DashboardCtrl', function ($scope, $http, $location, localStorageService, $rootScope, Lightbox, modalService, Auth, $window, $cookies, $sce, $mdDialog) {
    $scope.myInterval = 1;
   // $scope.CompanyID = 0;
    
    $scope.GetAllBanners = function () {
        //if ($scope.CompanyID == undefined) {
            
            $scope.ShowCompaniesList();
        //}
     

        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/Banners/GetActiveBanners?nocache=" + new Date().getTime(),
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            $scope.slides = data;
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });


    };

    $scope.ShowAddAnnouncement = function (id, title, description, categoryId, createdBy, createdOn) {
        id = id == undefined ? 0 : id;
        operationType = id == 0 ? "Add" : "Edit";
        $scope.editedAnnouncementId = id;
        $scope.editedAnnouncementTitle = title;
        $scope.editedAnnouncementDescription = description;
        $scope.editedAnnouncementCategoryId = categoryId;
        $scope.editedAnnouncementCreatedBy = createdBy;
        $scope.editedAnnouncementCreatedOn = createdOn;
        editorDescription = description;
        isRoleAdmin = false;
        $mdDialog.show({
            controller: 'AddAnnouncementCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/ReadMoreAnnouncement.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            scope: $scope,
            preserveScope: true,
            clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        })

    };

    $scope.ShowAddNews = function (id, title, description, category) {
        debugger;
        id = id == undefined ? 0 : id;
        operationType = id == 0 ? "Add" : "Edit";
        $scope.editedNewsId = id;
        $scope.editedNewsTitle = title;
        $scope.editedNewsDescription = description;
        $scope.editedNewsCategoryId = category;
        editorDescription = description;
        isRoleAdmin = false;
        $mdDialog.show({
            controller: 'AddNewsCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/ReadMoreNews.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            scope: $scope,
            preserveScope: true,
            clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        })
    };

    $scope.ShowAllDocuments = function (category) {

        $cookies.put("category", category.Id);
        $cookies.put("categoryName", category.Title);
        window.location.href = '#/DocumentList';

    };

    $scope.ReadMoreAnnouncement = function (announcement) {
        $mdDialog.show({
            controller: 'ReadMoreAnnouncementCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/AnnouncementReadMore.html',
            parent: angular.element(document.body),
            resolve: {
                items: function () {
                    return announcement;
                }
            },
            clickOutsideToClose: true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }), function () {

        };
    };

  
    $scope.ShowCompaniesList = function () {
        $scope.CompanyID = 0;
        $scope.Companies = [];
        var req = {
            method: 'POST',
            url: getBaseUrl() + '/Home/GetCompanies?nocache=' + new Date().getTime(),
            contentType: "application/json",
            scope: $scope,
            preserveScope: true
        }
        $http(req).success(function (data) {
            $scope.Companies = data;
            //$scope.CompanyID = data[1].Value;
            
        }).error(function (error) {
            modalService.show('Error While loading Data.', StatusType.ERROR);
        });
       
    }



    $scope.IsSuperUser = function () {
        //alert(Auth.userIsSuperUser("SuperUser"));
       return Auth.userIsSuperUser("SuperUser");
        
    }


    $scope.SaveUserSelectedCompany = function () {
        if ($scope.CompanyID != undefined) {
            
                
            
            var request = {
                method: 'POST',
                url: getBaseUrl() + '/Home/SaveUserSelectedCompany?nocache=' + new Date().getTime(),
                contentType: "application/json",
                data: { CompanyId: $scope.CompanyID },
                scope: $scope,
                preserveScope: true
            }
            $http(request).success(function (data) {
                window.location.href = '#/Dashboard';
                 location.reload(true);


                
            }).error(function (error) {
                modalService.show('Error Occurred while Saving.', StatusType.ERROR);
            });
        }
    }

   
    $scope.ReadMoreProjects = function (project) {
        $mdDialog.show({
            controller: 'ReadMoreProjectCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/ReadMoreProject.html',
            parent: angular.element(document.body),
            resolve: {
                items: function () {
                    return project;
                }
            },
            clickOutsideToClose: true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }), function () {

        };
    };

    $scope.ReadMoreBirthday = function (project) {
        $mdDialog.show({
            controller: 'ReadMoreBirthdayCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/ReadMoreBirthday.html',
            parent: angular.element(document.body),
            resolve: {
                items: function () {
                    return project;
                }
            },
            clickOutsideToClose: true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }), function () {

        };
    };

    $scope.ReadMore = function (event) {
        $mdDialog.show({
            controller: 'ReadMoreEventCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/EventReadMore.html',
            parent: angular.element(document.body),
            resolve: {
                items: function () {
                    return event;
                }
            },
            clickOutsideToClose: true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }), function () {

        };
    };

    $scope.ShowAllPhotos = function (album) {

        $cookies.put("photoAlbumId", album.Id);
        $cookies.put("photoAlbumName", album.Name);
        window.location.href = '#/PhotoGallery';

    };

    $scope.Dashboard = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/Banners/Dashboard?nocache=" + new Date().getTime(),
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            for (i = 0; i < data.Videos.length; i++) {
                data.Videos[i].Path = $sce.trustAsResourceUrl(data.Videos[i].Path);
            }

            $scope.announcements = data.Announcements;
            $scope.projects = data.Projects;
            $scope.birthdays = data.Birthdays;
            $scope.news = data.News;
            $scope.events = data.Events;
            $scope.documents = data.Documents;
            $scope.videos = data.Videos;
            $scope.photos = data.Photos;
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    };

    $scope.Login = function () {
        Auth.login();
    }


    $scope.signOut = function () {
        var auth2 = $window.gapi.auth2.getAuthInstance();

        auth2.signOut().then(function () {

            var get = $http({
                method: "GET",
                url: getBaseUrl() + "/Account/Logout",
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                Auth.logout();
                window.location = "/Account/Login";
            });

            get.error(function (data, status) {
                modalService.show("Error Occured", StatusType.ERROR);
            });

        });
    }

    $scope.openFromLeft = function (announcement) {
        $mdDialog.show(
          $mdDialog.alert()
            .clickOutsideToClose(true)
            .title(announcement.Title)
            .textContent(announcement.Description)
            .ariaLabel(announcement.CreatedBy)
            .ok('Nice!')
            // You can specify either sting with query selector
            .openFrom('#left')
            // or an element
            .closeTo(angular.element(document.querySelector('#right')))
        );
    };


    $scope.showPrerenderedDialog = function (ev, modalChar, obj) {
        debugger;
        var contentElem = modalChar + obj.Id;
        $mdDialog.show({
            contentElement: '#' + contentElem,
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        });
    };

    $scope.Login();


    setTimeout(
      function () {
          if (!Auth.isLoggedIn())
              window.location = getBaseUrl() + "/Account/Login";
          // $window.onSignIn = onSignIn;
          $scope.Dashboard();
          $scope.GetAllBanners();

      }, 500);

});
