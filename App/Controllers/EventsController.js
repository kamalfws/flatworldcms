﻿CMSapp.controller('EventsController', function ($http, $scope, $location, $mdSidenav, $mdDialog, modalService, $filter) {
    //$scope.
    //var year = 2016;

    //var today = new Date();
    //var currentYear = today.getFullYear();

    //var range = [];
    //range.push(year);
    //for (var i = 1; i < 7; i++) {
    //    range.push(year + i);
    //}
    //$scope.years = range;
    $scope.EventId = null;
    $scope.currentMonth = $filter('date')(new Date(), 'MMMM');
    //var get = $http({
    //    method: "GET",
    //    url: getBaseUrl() + "/Events/GetEvents",
    //    dataType: 'json',
    //    headers: { "Content-Type": "application/json" }
    //});

    //get.success(function (data, status) {
    //    $scope.events = data.events;
    //    $scope.year = data.year;
    //    debugger;
    //    $scope.GetVisibility($scope.currentMonth);
    //});

    //get.error(function (data, status) {
    //    modalService.show("Error Occured", StatusType.ERROR);
    //});

    $scope.GetAllEvents = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/Events/GetEvents",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            for (var i = 0; i < data.events.length; i++) {
                data.events[i].StartDateFormat = moment(data.events[i].StartDateFormat).format("DD/MM/YYYY");
            }
            $scope.events = data.events;
            $scope.year = data.year;
            $scope.GetVisibility($scope.currentMonth);
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });

    };

    $scope.$watch('currentMonth', function (val) {
        $scope.GetVisibility(val);
    });

    $scope.GetVisibility = function (val) {
        if (val != 'All') {
            var items = $filter('filter')($scope.events, val);
            $scope.hasData = items.length > 0 ? true : false;
        }


    }

    $scope.GetAllEvents();

    $scope.ReadMore = function (event) {
        $mdDialog.show({
            controller: 'ReadMoreEventCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/EventReadMore.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            resolve: {
                items: function () {
                    return event;
                }
            },
            clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }), function () {

        };
    };

    $scope.ShowAddEvent = function (event) {
        if (event != undefined) {
            $scope.Name = event.Name;
            $scope.Location = event.Location;
            $scope.Description = event.Description;
            $scope.StartDate = moment(event.StartDate)._d;
            $scope.StartDateFormat = moment(event.StartDate).format("DD/MM/YYYY")
            $scope.file = event.Image;
            $scope.Editable = true;
            $scope.EventId = event.EventId;
            $scope.EventMinDate = $scope.StartDate;
        }
        else {
            $scope.Name = $scope.Location = $scope.Description = $scope.StartDate = $scope.file = $scope.EventId = null;
            $scope.Editable = false;
            $scope.EventMinDate = new Date();
        }
        $mdDialog.show({
            controller: 'AddEventCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/AddEvent.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            scope: $scope,
            preserveScope: true,
            clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function (answer) {
            if (answer.toLowerCase() == "success") {
                $scope.GetAllEvents();
                modalService.show("Successfully Uploaded", StatusType.SUCCESS);
            }
            else
                modalService.show("Error Occured", StatusType.ERROR);
            //$scope.status = 'You said the information was "' + answer + '".';
        }, function () {

            });
        
    };

    //$scope.EditEvent = function (event) {
    //    $mdDialog.show({
    //        controller: 'EditEventCtrl',
    //        templateUrl: getBaseUrl() + '/App/Partials/EditEvent.html',
    //        parent: angular.element(document.body),
    //        resolve: {
    //            items: function () {
    //                //we can send data from here to controller using resolve...
    //                return event;
    //            }
    //        },
    //        // targetEvent: ev,
    //        clickOutsideToClose: false,
    //        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    //    }).then(function (answer) {
    //        if (answer.toLowerCase() == "success") {
    //            $scope.GetAllEvents();
    //            modalService.show("Successfully Uploaded", StatusType.SUCCESS);
    //        }
    //        else
    //            modalService.show("Error Occured", StatusType.ERROR);
    //        //$scope.status = 'You said the information was "' + answer + '".';
    //    }, function () {

    //    });
    //};

    $scope.DeleteEvent = function (event) {
        var confirm = $mdDialog.confirm()
          .title('Delete Event')
          .textContent('Do you really want to delete this Event?')
          .ok('Yes')
          .cancel('No');

        $mdDialog.show(confirm).then(function () {
            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/Events/DeleteEvent",
                data: { eventId: event.EventId },
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                $scope.GetAllEvents();
                modalService.show("Document Removed", StatusType.SUCCESS);
            });

            get.error(function (data, status) {
                modalService.show("Error Occured", StatusType.ERROR);
            });
        });
    };

});