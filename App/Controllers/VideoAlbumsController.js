﻿CMSapp.controller('VideoAlbumsController', function ($scope, $http, $location, localStorageService, $rootScope, $sce, $mdDialog, modalService, Upload,$cookies) {
   
       var get = $http({
        method: "GET",
        url: getBaseUrl() + "/Video/GetVideoAlbums",
        dataType: 'json',
        headers: { "Content-Type": "application/json" }
    });

    get.success(function (data, status) {
        $scope.albums = data;

    });

    get.error(function (data, status) {
        modalService.show("Error Occured", StatusType.ERROR);
    });

    $scope.GetVideoAlbums = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/Video/GetVideoAlbums",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            $scope.albums = data;

        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });

    };

    $scope.ShowAllVideos = function (album) {
        $cookies.put("videoAlbumId", album.Id);
        $cookies.put("videoAlbumName", album.Name);
        window.location.href = '#/VideoGallery';
    };

    var self = this;
    $scope.status = '';
    $scope.customFullscreen = false;
    $scope.ShowAddVideoAlbum = function () {
        $mdDialog.show({
            controller: 'VideoAlbumsController',
            templateUrl: getBaseUrl() + '/App/Partials/AddVideoCategory.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function (answer) {
            if (answer.toLowerCase() == "success") {
                $scope.GetVideoAlbums();
                modalService.show("Successfully Uploaded", StatusType.SUCCESS);
            }
            else
                modalService.show("Error Occured", StatusType.ERROR);
            //$scope.status = 'You said the information was "' + answer + '".';
        }, function () {

        });
    };

    $scope.ShowEditVideoAlbum = function (album) {
        debugger;
        $scope.Name = album.Name;
            $scope.Id = album.Id;
            $mdDialog.show({
            controller: 'VideoAlbumsController',
            templateUrl: getBaseUrl() + '/App/Partials/EditVideoCategory.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            scope: $scope,
            preserveScope: true,

            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function () {
            
            //$scope.status = 'You said the information was "' + answer + '".';
        }, function () {

        });
    };


    $scope.SaveAlbum = function () {
        debugger;
        if ($scope.Name != "undefined") {
            $scope.upload();
        }
        else { modalService.show("Please enter category Name", StatusType.ERROR); }
    }

    $scope.validateUrl = function () {
        var link = $scope.link;
        var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
        if (!re.link(url)) {
            $scope.link = "";
        }
    };

    // upload on file select or drop
    $scope.upload = function () {
        Upload.upload({
            url: getBaseUrl() + '/Video/SaveVideoAlbum',
            data: { 'Name': $scope.Name }
        }).then(function (resp) {
            $scope.Answer("Success");
        }, function (resp) {
            $scope.Answer("Failure");
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        });
    };

    $scope.Cancel = function () {
        $mdDialog.cancel();
    };

    $scope.Answer = function (status) {
        $mdDialog.hide("success");
    }

    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.answer = function (answer) {
        $mdDialog.hide(answer);
    };
    //======================

    $scope.EditAlbum = function () {
        debugger;
      var get = $http({
                method: "POST",
                url: getBaseUrl() + "/Video/SaveVideoAlbum",
                data: { 'Name': $scope.Name, Id: $scope.Id },
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                $scope.GetVideoAlbums();
                modalService.show("Video Album Updated", StatusType.SUCCESS);
                $scope.Cancel();
            });

            get.error(function (data, status) {
                modalService.show("Error Occured", StatusType.ERROR);
            });
     
    };


    $scope.DeleteAlbum = function (album) {
        var confirm = $mdDialog.confirm()
         .title('Delete Photo Album')
         .textContent('Do you want to delete this Video Album?')
         .ok('Yes')
         .cancel('No');
        $mdDialog.show(confirm).then(function () {
            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/Video/DeleteVideoAlbum",
                data: { videoalbumId: album.Id },
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                $scope.GetVideoAlbums();
                modalService.show("Video Album Removed", StatusType.SUCCESS);
            });

            get.error(function (data, status) {
                modalService.show("Error Occured", StatusType.ERROR);
            });
        });
    };

    $scope.GetVideoAlbums();
});


