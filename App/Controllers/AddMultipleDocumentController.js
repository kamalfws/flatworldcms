﻿CMSapp.controller('AddMultipleDocumentCtrl', function ($scope, Upload, $mdDialog, $mdSidenav, modalService, $cookies) {
    $scope.docCategory = $cookies.get('categoryName');
    $scope.docCategoryID = $cookies.get('category');
   
    $scope.SaveDocument = function () {
        if ($scope.file) {
            $scope.upload($scope.file);
        }
        else
            modalService.show("Please Upload Valid Files", StatusType.ERROR);
    }
    // upload on file select or drop
    $scope.upload = function (file) {
        Upload.upload({
            url: getBaseUrl() + '/Document/SaveMultipleDocuments',
            data: { 'files': file, 'categoryID': $scope.docCategoryID, 'categoryName': $scope.docCategory }
        }).then(function (resp) {
            $scope.Answer("Success");
        }, function (resp) {
            $scope.Answer("Failure");
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        });
    };

    $scope.Cancel = function () {
        $mdDialog.cancel();
    };

    $scope.Answer = function (status) {
        $mdDialog.hide("success");
    }

  
    

});