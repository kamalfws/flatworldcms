﻿CMSapp.controller('ReadMoreBirthdayCtrl', function ($scope, $mdDialog, items) {

    $scope.Name = items.Name;
    $scope.BirthDate = items.BirthDate;
    $scope.ProfilePicPath = items.ProfilePicPath;
    $scope.Designation = items.Designation;
    $scope.Email = items.Email;
    $scope.Phone = items.Phone;

    $scope.Cancel = function () {
        $mdDialog.cancel();
    };

    $scope.Answer = function (status) {
        $mdDialog.hide("success");
    }
});