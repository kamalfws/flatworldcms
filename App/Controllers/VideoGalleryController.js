﻿//add embeded video
CMSapp.controller('VideoGalleryCtrl', ['$scope', '$http', '$location', 'localStorageService', '$rootScope', 'Lightbox', '$cookies', '$timeout', 'modalService', '$mdDialog', 'Upload', '$sce', function ($scope, $http, $location, localStorageService, $rootScope, Lightbox, $cookies, $timeout, modalService, $mdDialog, Upload, $sce) {

    $scope.albumName = $cookies.get('videoAlbumName');
    $scope.videoAlbumId = $cookies.get('videoAlbumId');
    ////var get = $http({
    ////    method: "GET",
    ////    url: getBaseUrl() + "/Video/GetVideos?ID=" + $scope.videoAlbumId,
    ////    dataType: 'json',
    ////    headers: { "Content-Type": "application/json" }
    ////});

    ////get.success(function (data, status) {

    ////    //for (i = 0; i < data.length; i++) {
    ////    //    data[i].url = $sce.trustAsResourceUrl(data[i].url);
    ////    //}


    ////    $scope.videos = data;
       

    ////    $(".video-gallery").click(function () {
    ////        console.log($(this))
    ////    });
    ////});

    //get.error(function (data, status) {
    //    modalService.show("Error Occured", StatusType.ERROR);
    //});


    $scope.GetVideos = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/Video/GetVideos?ID=" + $scope.videoAlbumId,
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            //for (i = 0; i < data.length; i++) {
            //    data[i].url = $sce.trustAsResourceUrl(data[i].url);
            //}
            $scope.videos = data;

            var vimeoCount = 0, vimeoImageCount=0;
            setTimeout(function () {
                var arr = [];
                $(".video-image").each(function () {
                    var url = $(this).attr("data-url");
                    var dataId = $(this).attr("data-id");
                    if (url.indexOf("vimeo") != -1) {
                        var match = /vimeo.*\/(\d+)/i.exec(url);
                        if (match) {
                            var vimeoVideoID = match[1];
                            vimeoCount++;
                            $.get('http://vimeo.com/api/v2/video/' + vimeoVideoID + '.json', function (data) {
                                arr.push({ imgUrl: data[0].thumbnail_large, vimeoVideoID: vimeoVideoID, url: url, dataId: dataId })
                                if (vimeoCount == ++vimeoImageCount)
                                    setVimeoTumbnail(arr);
                            })
                        }
                    }
                    else {
                        $(this).attr("src", getYoutubeVideoImage(url));
                        var id = $(this).attr("data-id");
                        $("[data-loading-url='" + url + "+" +id + "']").remove();
                    }
                });

                setTimeout(function () {
                    $("[data-url]").each(function () {
                        var url = $(this).attr("data-url");
                        var id = $(this).attr("data-id");

                        if (url.indexOf("vimeo") == -1) {
                            $(this).attr({ "video-type": "youtube" });
                           
                        }
                    });
                }, 1000);

                var id, url;
                $(".video-image, .fa-youtube-play").hover(function () {
                    id = $(this).attr("data-id");
                    url = $(this).attr("data-url");
                    videoType = $(this).attr("video-type");

                    $("[data-video-play-url='" + url + "+" + id + "']").show();
                    if (videoType == "vimeo")
                        $("[data-video-play-url='" + url + "+" + id + "']").css("color", "rgb(0, 173, 239)");
                    else
                        $("[data-video-play-url='" + url + "+" + id + "']").css("color", "#da2625");
                }, function () {
                    id = $(this).attr("data-id");
                    url = $(this).attr("data-url");
                    videoType = $(this).attr("video-type");

                    $("[data-video-play-url='" + url + "+" + id + "']").hide();
                })

                //$(".video-hover").hover(function () {
                //    $(this).css("opacity", "0.4");
                //}, function () {
                //    $(this).css("opacity", "1");
                //})
            }, 1000);
            
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });

    };

   
    $scope.GetVideos();

    $scope.showVideoPopup = function (url, caption) {
        $scope.videoCaption = caption;
        $scope.videoUrl = url;
        
        $mdDialog.show({
            contentElement: '#divShowPopup',
            parent: angular.element(document.body),
            scope: $scope,
            preserveScope: true,
            clickOutsideToClose: false,
        });
        var videoType = "", videoId ="";
        $("[data-url='" + url+"']").each(function () {
            videoType = $(this).attr("video-type");
            if(videoType=="vimeo")
                videoId = $(this).attr("vimeo-id");
        });
        if (videoType == "vimeo")
            url = "https://player.vimeo.com/video/" + videoId;
        $("#iframe").attr("src", url).height(400);
    }

    $scope.validateURL = function () {
        if ($scope.link.length != 0) {
            var link = $scope.link;
            var re = /^(http:\/\/|https:\/\/)(player\.vimeo\.com|vimeo\.com|youtu\.be|www\.youtube\.com)\/([\w\/]+)([\?].*)?$/;
            if (!re.test(link)) {
                modalService.show("Please enter a valid Youtube or Vimeo link", StatusType.ERROR);
                $scope.link = "";
            }
        }
    };

    var self = this;
    $scope.status = '';
    $scope.customFullscreen = false;
    $scope.ShowAddVideo = function () {
        $mdDialog.show({
            controller: 'VideoGalleryCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/CustomDialog.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function (answer) {
            if (answer.toLowerCase() == "success") {
                $scope.GetVideos();
                modalService.show("Successfully Uploaded", StatusType.SUCCESS);
            }
            else
                modalService.show("Error Occured", StatusType.ERROR);
            //$scope.status = 'You said the information was "' + answer + '".';
        }, function () {

        });
    };


    $scope.ShowEditVideo = function (video) {
        debugger;
        $scope.Caption = video.caption;
        $scope.link = video.url;
       
        $scope.Id = video.Id;
        $mdDialog.show({
            controller: 'VideoGalleryCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/EditDialog.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            scope: $scope,
            preserveScope: true,

            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function () {
            
           //$scope.status = 'You said the information was "' + answer + '".';
        }, function () {

        });
    };


    $scope.SaveVideo = function () {
        $scope.validateURL();
        if ($scope.Caption != undefined && $scope.Caption != "" && $scope.link != undefined && $scope.link != "") {
            $scope.upload();
        }
        else {
            modalService.show("Please enter all mandatory fields", StatusType.ERROR);
            //if ($scope.file && $scope.file.type != "video/mp4") {
            //    modalService.show("Allows only .mp4", StatusType.ERROR);
            //}
            //else {
            //    modalService.show("Please enter all mandatory fields", StatusType.ERROR);
            //}
        }
    }
    // upload on file select or drop
    $scope.upload = function () {
        Upload.upload({
            url: getBaseUrl() + '/Video/SaveVideo',
            data: { 'name': $scope.Caption, 'url': $scope.link, 'albumID': $scope.videoAlbumId }
        }).then(function (resp) {
            
            $scope.Answer("Success");
        }, function (resp) {
            $scope.Answer("Failure");
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        });
    };

    $scope.Cancel = function () {
        $mdDialog.cancel();
    };

    $scope.Answer = function (status) {
        $mdDialog.hide("success");
    }

    $scope.EditVideo = function () {
        debugger;
          var get = $http({
                method: "POST",
                url: getBaseUrl() + "/Video/SaveVideoName",
                data: { Name: $scope.Caption, Id: $scope.Id},
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                $scope.GetVideos();
                modalService.show("Video Caption Edited", StatusType.SUCCESS);
                $scope.Cancel();
            });

            get.error(function (data, status) {
                modalService.show("Error Occured", StatusType.ERROR);
            });
       
    };

    $scope.DeleteVideo = function (video) {
        var confirm = $mdDialog.confirm()
            .title('Delete Video')
            .textContent('Do you want to delete this Video?')
            .ok('Yes')
            .cancel('No');

        $mdDialog.show(confirm).then(function () {
            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/Video/DeleteVideo",
                data: { videoId: video.Id },
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                $scope.GetVideos();
                modalService.show("Video Removed", StatusType.SUCCESS);
            });

            get.error(function (data, status) {
                modalService.show("Error Occured", StatusType.ERROR);
            });
        });
    };

}]);

function setVimeoTumbnail(arr) {
    for (var i = 0; i < arr.length; i++) {
        $("[data-id='" + arr[i].dataId + "']").attr({ "src": arr[i].imgUrl, "video-type": "vimeo", "vimeo-id": arr[i].vimeoVideoID });
        $("[data-loading-url='" + arr[i].url + "+" + arr[i].dataId + "']").remove();
    }
}


function getYoutubeVideoImage(url) {
    var i, r, rx = /^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))([^#\&\?]*).*/;
    r = url.match(rx);
    var imgurl = 'http://img.youtube.com/vi/' + r[1] + '/1.jpg';
    return imgurl;
}
