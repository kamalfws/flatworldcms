﻿

CMSapp.controller('ProjectController', function ($http, $scope, $location, $mdSidenav, $mdDialog, Auth, modalService) {
    debugger;
    $scope.filterBy = '';
   
    $scope.pageSizeList = getPageSizeList();
    $scope.pPageSizeObj = PageSizeSelectedObj;
    $scope.PageSize = $scope.pPageSizeObj.selected;
    if (Auth.userHasPermission('Administrator'))
    {
        $scope.Role="Administrator";
    $scope.show_fields = ['Name', 'Owner','Description'];
    }
    else
    {
        $scope.show_fields = ['Name', 'Owner', 'Description'];
         $scope.Role = "User";
    }

    $scope.TableType = 'Project';
    $scope.Id = 0;
  

    //Called from on-data-required directive.
    $scope.onServerSideItemsRequested = function (currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        LoadProjectData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse);
    }

    // Called from global search box
    $scope.onChange = function () {
        LoadProjectData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Called from page size drop down changed.
    $scope.changePageSize = function () {
        $scope.PageSize = $scope.pPageSizeObj.selected;
    }

    $scope.clearSearchParams = function () {
        $scope.filterBy = '';
        LoadProjectData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Ajax call  to fetch data from server...
    function LoadProjectData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        var userSearch = {
            currentPage: currentPage,
            pageItems: pageItems,
            filterBy: filterBy,
            filterByFields: $scope.Role ,
            orderBy: orderBy,
            orderByReverse: orderByReverse
        }
        pCurrentPage = currentPage;
        pPageItems = pageItems;
        pOrderBy = orderBy;
        pOrderByReverse = orderByReverse
        var req = {
            method: 'POST',
            url: getBaseUrl() + '/Project/GetProjects?nocache=' + new Date().getTime(),
            contentType: "application/json",
            data: JSON.stringify(userSearch)
        }
        $http(req).success(function (data) {
            debugger;
            $scope.Projects = data;
            $scope.TotalRecordsCount = data[0].TotalCount;
        }).error(function (error) {
            modalService.show(StatusType.ERROR);
        });
    }

    $scope.showEnableDisableConfirm = function (gridItem, ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var original = !gridItem.Active;
        var confirm = $mdDialog.confirm()
            .title((gridItem.Active == false ? 'Disable' : 'Enable') + ' ' + 'Project')
            .textContent('Are you really want to' + ' ' + (gridItem.Active == false ? 'Disable' : 'Enable') + ' this Project?')
            .ok('Yes')
            .cancel('No');

        $mdDialog.show(confirm).then(function () {
            var req = {
                method: 'POST',
                url: getBaseUrl() + '/Project/EnableDisableProject?nocache=' + new Date().getTime(),
                contentType: "application/json",
                data: {
                    projectID: gridItem.Zohoprojectid,
                    enable: gridItem.Active
                }
            }
            $http(req).success(function (data) {
                if (data == "Success") {
                    modalService.show('Project Is' + (gridItem.Active == false ? 'Disabled' : 'Enabled'), StatusType.SUCCESS);
                }
                else if (data == "OnlyOneAdmin") {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .title('Alert')
                        .textContent('UserDisableWarning')
                        .ok('Ok')
                    );
                    gridItem.Active = original;
                }
                else {
                    modalService.show('Project Is ' + (gridItem.Active == false ? 'Disable' : 'Enable') + 'FailedMsg', StatusType.ERROR);
                    gridItem.Active = original;
                }
            }).error(function (error) {
                modalService.show('ErrorMsgWhileLoading', StatusType.ERROR);
            });
        }, function () {
            gridItem.Active = original;
        });
    };
    
});