﻿CMSapp.controller('PhotoAlbumsController', function ($http, $scope, $location, $mdSidenav, $mdDialog, $cookies, modalService) {
    debugger;
    //var get = $http({
    //    method: "GET",
    //    url: getBaseUrl() + "/Photo/GetPhotoAlbums",
    //    dataType: 'json',
    //    headers: { "Content-Type": "application/json" }
    //});
    //get.success(function (data, status) {
    //    $scope.albums = data;
    //});
    //get.error(function (data, status) {
    //    modalService.show("Error Occured", StatusType.ERROR);
    //});

    $scope.GetPhotoAlbums = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/Photo/GetPhotoAlbums",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });
        get.success(function (data, status) {
            $scope.albums = data;
        });
        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });

    };

    $scope.ShowAddPhotoAlbum = function () {
        $mdDialog.show({
            controller: 'AddPhotoAlbumCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/AddPhotoAlbum.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            // targetEvent: ev,
             clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function (answer) {
            if (answer.toLowerCase() == "success") {
                $scope.GetPhotoAlbums();
                modalService.show("Successfully Uploaded", StatusType.SUCCESS);
            }
            else
                modalService.show("Error Occured", StatusType.ERROR);
            //$scope.status = 'You said the information was "' + answer + '".';
        }, function () {

        });
    };


    $scope.ShowAllPhotos = function (album) {

        $cookies.put("photoAlbumId", album.Id);
        $cookies.put("photoAlbumName", album.Name);
        window.location.href = '#/PhotoGallery';

    };

    $scope.DeleteAlbum = function (album) {
        var confirm = $mdDialog.confirm()
         .title('Delete Photo Album')
         .textContent('Do you want to delete this Photo Album?')
         .ok('Yes')
         .cancel('No');

        $mdDialog.show(confirm).then(function () {
            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/Photo/DeletePhotoAlbum",
                data: { photoalbumId: album.Id },
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                $scope.GetPhotoAlbums();
                modalService.show("Photo  Album Removed", StatusType.SUCCESS);
            });

            get.error(function (data, status) {
                modalService.show("Error Occured", StatusType.ERROR);
            });
        });
    };

    $scope.GetPhotoAlbums();

});


