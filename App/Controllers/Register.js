﻿CMSapp.controller('Register', function ($http, $scope, $location, $mdSidenav, $mdDialog, modalService, Auth) {
    //$scope.Save = function () {
    //    if ($scope.form.file.$valid && $scope.file) {
    //        $scope.upload($scope.file);
    //    }
    //    else
    //        modalService.show("Please Upload image Files", StatusType.ERROR);
    //}
    // upload on file select or drop
    $scope.Save = function () {
        if ($scope.Registrationform.$valid) {
            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/Register/UserRegistration",
                data: {
                    "CompanyName": $scope.CompanyName,
                    "CompanyEmail": $scope.CompanyEmail, 
                    "Address": $scope.Address,
                    "Zipcode": $scope.Zipcode,
                    "FirstName":$scope.FirstName,
                    "Mobile": $scope.Mobile,
                    "UserEmail":$scope.UserEmail
                },
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });
        
            get.success(function (data, status) {
                if (data == "success") {
                    $scope.GetNews();
                    $mdDialog.hide();
                    modalService.show('Successfully Registered', StatusType.SUCCESS);
                 
                }
                else {
                    $scope.Answer("Failure");
                }
                $mdDialog.hide(status);
            });

            get.error(function (data, status) {
                $scope.Answer("Failure");
            });
      }
    }


    $scope.clearFields = function () {
        $scope.CompanyName = $scope.CompanyEmail = $scope.Address = $scope.Zipcode = $scope.FirstName = $scope.Mobile = $scope.UserEmail = '';
       
        $scope.BatteriesInfodiv.$setPristine();
        $scope.BatteriesInfodiv.$setUntouched();
    }
});