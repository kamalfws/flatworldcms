﻿CMSapp.controller('AnnouncementCtrl', function ($scope, $http, $mdDialog, modalService, Auth, $sce, $filter) {
    $scope.currentPage = 0;
    $scope.pageSize = 10;

    $scope.ShowAddCategory = function (editedCategoryId, editedCategoryName) {
        $scope.announcementCategoryForm.$setPristine();
        $scope.announcementCategoryForm.$setUntouched();
        $scope.editedCategoryId = editedCategoryId;
        $scope.editedCategoryName = editedCategoryName;
        $mdDialog.show({
            contentElement: '#addCategoryDialog',
            parent: angular.element(document.body)
        });
    }

    $scope.ChangeCategoryStatus = function (statusModifiedCategoryId) {

        var confirm = $mdDialog.confirm()
         .title('Please confirm to change the status')
         .ariaLabel('Confirm')
         .ok('Yes')
         .cancel('No');

        $mdDialog.show(confirm).then(function () {

            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/Announcement/ChangeCategoryStatus",
                data: { id: statusModifiedCategoryId },
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                modalService.show("Updated Category Status", StatusType.SUCCESS);
                $scope.GetAnnouncements();
            });

            get.error(function (data, status) {
                modalService.show("Error Occured", StatusType.ERROR);
            });

        }, function () {
        });
    }

    $scope.AddCategoryCancel = function () {
        $mdDialog.hide();
    }

    $scope.SaveAnnouncementCategory = function () {
        var get = $http({
            method: "POST",
            url: getBaseUrl() + "/Announcement/SaveAnnouncementCategory",
            data: { "name": $scope.editedCategoryName, "categoryId": $scope.editedCategoryId == undefined ? 0 : $scope.editedCategoryId },
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            if (data == "Success") {
                modalService.show("Category Added", StatusType.SUCCESS);
                $scope.GetAnnouncements();
                $mdDialog.hide();
            }
            else
                modalService.show("Category Exists", StatusType.ERROR);
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });


    }

    $scope.ShowAddAnnouncement = function (id, title, description, categoryId, createdBy, createdOn) {
        id = id == undefined ? 0 : id;
        operationType = id == 0 ? "Add" : "Edit";
        $scope.editedAnnouncementId = id;
        $scope.editedAnnouncementTitle = title;
        $scope.editedAnnouncementDescription = description;
        $scope.editedAnnouncementCategoryId = categoryId;
        $scope.editedAnnouncementCreatedBy = createdBy;
        $scope.editedAnnouncementCreatedOn = createdOn;
        editorDescription = description;
        isRoleAdmin = $scope.IsAdmin();
        $mdDialog.show({
            controller: 'AddAnnouncementCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/AddAnnouncement.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            scope: $scope,
            preserveScope: true,
            clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function (answer) {
            if (answer.toLowerCase() == "success") {
                modalService.show("Successfully " + ($scope.editedNewsId == 0 ? "Added" : "Updated") + " Announcement", StatusType.SUCCESS);
                $scope.GetAnnouncements();
            }
            else if (answer.toLowerCase() == "cancel") {

            }
            else {
                modalService.show("Error Occured", StatusType.ERROR);
            }
            $scope.ResetSelectedAnnouncement();
        }, function () {

        });

    };

    $scope.ResetSelectedAnnouncement = function () {
        id = 0;
        $scope.editedAnnouncementId = 0;
        $scope.editedAnnouncementTitle = undefined;
        $scope.editedAnnouncementDescription = undefined;
        $scope.editedAnnouncementCategoryId = undefined;
    };

    $scope.IsAdmin = function () {
        return Auth.userHasPermission("Administrator");
    }

    $scope.GetAnnouncements = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/Announcement/GetAnnonuncements",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            $scope.categories = data.categories;
            $scope.announcements = data.announcements;
            $scope.dataLength = data.announcements.length;
            $filter('filter')($scope.announcements, $scope.selectedCategoryId)
            formatTextToHtml();
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    };

    $scope.SelectCategory = function (categoryId) {
        $scope.selectedCategoryId = categoryId;
        setTimeout(function () {
            $(".news-description").each(function () {
                $(this).html($(this).text());
            });
        }, 100);
    };

    $scope.ChangeStatus = function (id) {

        var confirm = $mdDialog.confirm()
         .title('Please confirm to change the status')
         .ariaLabel('Confirm')
         .ok('Yes')
         .cancel('No');

        $mdDialog.show(confirm).then(function () {
            $scope.StatusChange(id);
        }, function () {
        });
    }

    $scope.trustAsHtml = $sce.trustAsHtml;
    $scope.StatusChange = function (id) {
        var get = $http({
            method: "POST",
            url: getBaseUrl() + "/Announcement/ChangeStatus",
            data: { id: id },
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            $scope.GetAnnouncements();
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    }

    $scope.numberOfPages = function () {
        return Math.ceil(($scope.selectedCategoryId != undefined ? $scope.filterResult.length : ($scope.announcements != undefined ? $scope.announcements.length : 0)) / $scope.pageSize);
    }


    $scope.$watch('selectedCategoryId', function (val) {
        $scope.currentPage = 0;
    });


    $scope.GetAnnouncements();

    $scope.PagingPrevChange = function () {
        $scope.currentPage = $scope.currentPage - 1;
        formatTextToHtml();
    }

    $scope.PagingNextChange = function () {
        $scope.currentPage = $scope.currentPage + 1;
        formatTextToHtml();
    }

});

//to display HTML view for news description
function formatTextToHtml() {
    setTimeout(function () {
        $(".news-description").each(function () {
            $(this).html($(this).text());
        });
    }, 1000);
}

