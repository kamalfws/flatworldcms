﻿CMSapp.controller('AddVideoCtrl', function ($scope, Upload, $mdDialog, $mdSidenav, modalService, $timeout) {
    debugger;
    
    $scope.SaveVideo = function () {
        if ($scope.file) {
            $scope.upload($scope.file);
        }
        else
            modalService.show("Please Upload Vedio Files", StatusType.ERROR);
    }
    // upload on file select or drop
    $scope.upload = function (file) {
        Upload.upload({
            url: getBaseUrl() + '/Video/SaveVideo',
            data: { 'file': file }
        }).then(function (resp) {
            debugger;
            $scope.Answer("Success");
        }, function (resp) {
            $scope.Answer("Failure");
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        });
    };

    $scope.Cancel = function () {
        $mdDialog.cancel();
    };

    $scope.Answer = function (status) {
        $mdDialog.hide("success");
    }

    $scope.validate = function () {
       
    }

       $scope.uploadFiles = function (file, errFiles) {
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: getBaseUrl() + '/Video/SaveVideo',
                data: { file: file }
            });

            file.upload.then(function (response) {

                $timeout(function () {
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                                         evt.loaded / evt.total));
            });
        }
    };

});
