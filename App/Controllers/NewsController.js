﻿CMSapp.controller('NewsCtrl', function ($scope, $http, $mdDialog, modalService, Auth, $filter, $sce) {
    $scope.currentPage = 0;
    $scope.pageSize = 10;

    $scope.toggleGroup = function (group) {
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };

    $scope.isGroupShown = function (group) {
        return $scope.shownGroup === group;
    };

    $scope.ShowAddCategory = function (editedCategoryId, editedCategoryName) {
        $scope.newsCategoryForm.$setPristine();
        $scope.newsCategoryForm.$setUntouched();
        $scope.editedCategoryId = editedCategoryId;
        $scope.editedCategoryName = editedCategoryName;
        $mdDialog.show({
            contentElement: '#addCategoryDialog',
            parent: angular.element(document.body)
        });
    }

    $scope.AddCategoryCancel = function () {
        $mdDialog.hide();
    }

    $scope.SaveNewsCategory = function () {
        var get = $http({
            method: "POST",
            url: getBaseUrl() + "/News/SaveNewsCategory",
            data: { "name": $scope.editedCategoryName, "categoryId": $scope.editedCategoryId == undefined ? 0 : $scope.editedCategoryId },
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            if (data == "Success") {
                modalService.show("Category Added", StatusType.SUCCESS);
                $scope.GetNews();
                $mdDialog.hide();
            }
            else
                modalService.show("Category Exists", StatusType.ERROR);
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    }

    $scope.ShowAddNews = function (id, title, description, categoryId) {
        id = id == undefined ? 0 : id;
        operationType = id == 0 ? "Add" : "Edit";
        $scope.editedNewsId = id;
        $scope.editedNewsTitle = title;
        $scope.editedNewsDescription = description;
        $scope.editedNewsCategoryId = categoryId;
        editorDescription = description;
        isRoleAdmin = $scope.IsAdmin();
        $mdDialog.show({
            controller: 'AddNewsCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/AddNews.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            scope: $scope,
            preserveScope: true,
            clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function (answer) {
            if (answer.toLowerCase() == "success") {
                modalService.show("Successfully " + (operationType =="Add"? "Added":"Updated")+ " News", StatusType.SUCCESS);
                $scope.GetNews();
            }
            else if (answer.toLowerCase() == "cancel") {

            }
            else {
                modalService.show("Error Occured", StatusType.ERROR);
            }
            $scope.ResetSelectedNews();
            //$scope.status = 'You said the information was "' + answer + '".';
        }, function () {

        });
    };

    $scope.ResetSelectedNews = function () {
        id = 0;
        $scope.editedNewsId = 0;
        $scope.editedNewsTitle = undefined;
        $scope.editedNewsDescription = undefined;
        $scope.editedNewsCategoryId = undefined;
    };

    $scope.GetNews = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/News/GetNews",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            $scope.categories = data.categories;
            $scope.news = data.news;
            $filter('filter')($scope.news, $scope.selectedCategoryId);
            formatTextToHtml();
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    };

    $scope.IsAdmin = function () {
        return Auth.userHasPermission("Administrator");
    }

    $scope.SelectCategory = function (categoryId) {
        $scope.selectedCategoryId = categoryId;
        setTimeout(function () {
            $(".news-description").each(function () {
                $(this).html($(this).text());
            });
        }, 100);
    };

    $scope.ChangeStatus = function (newsRecord) {
        var id = newsRecord.Id;
        var original = !newsRecord.IsActive;
        var confirm = $mdDialog.confirm()
         .title('Please confirm to change the status')
         .ariaLabel('Confirm')
         .ok('Yes')
         .cancel('No');
        $mdDialog.show(confirm).then(function () {
            $scope.StatusChange(id);
        }, function () {
            newsRecord.IsActive = original;
        });
    }

    $scope.PagingPrevChange = function () {
        $scope.currentPage = $scope.currentPage - 1;
        formatTextToHtml();
    }

    $scope.PagingNextChange = function () {
        $scope.currentPage = $scope.currentPage + 1;
        formatTextToHtml();
    }

    $scope.StatusChange = function (id) {
        var get = $http({
            method: "POST",
            url: getBaseUrl() + "/News/ChangeStatus",
            data: { id: id },
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            //$scope.GetNews();
            //formatTextToHtml();
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    }

    $scope.numberOfPages = function () {
        return Math.ceil(($scope.selectedCategoryId != undefined ? $scope.filterResult.length : ($scope.news != undefined ? $scope.news.length : 0)) / $scope.pageSize);
    }

    $scope.$watch('selectedCategoryId', function (val) {
        $scope.currentPage = 0;
    });

    $scope.GetNews();

});

//to display HTML view for news description
function formatTextToHtml() {
    setTimeout(function () {
        $(".news-description").each(function () {
            $(this).html($(this).text());
        });
    }, 1000);
}
