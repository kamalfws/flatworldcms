﻿CMSapp.controller('DocumentController', function ($http, $scope, $location, $mdSidenav, $mdDialog, $cookies, modalService) {

    var get = $http({
        method: "GET",
        url: getBaseUrl() + "/Document/GetDocCategories",
        dataType: 'json',
        headers: { "Content-Type": "application/json" }
    });

    get.success(function (data, status) {
        $scope.categories = data;

    });

    get.error(function (data, status) {
        modalService.show("Error Occured", StatusType.ERROR);
    });

    $scope.GetAllDocumentCategories = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/Document/GetDocCategories",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            $scope.categories = data;

        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });

    };

    $scope.ShowAddDocument = function () {
        $mdDialog.show({
            controller: 'AddDocumentCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/AddDocument.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            // targetEvent: ev,
             clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function (answer) {
            if (answer.toLowerCase() == "success") {
                $scope.GetAllDocumentCategories();
                modalService.show("Successfully Uploaded", StatusType.SUCCESS);
            }
            else
                modalService.show("Error Occured", StatusType.ERROR);
            //$scope.status = 'You said the information was "' + answer + '".';
        }, function () {

        });
    };


    $scope.ShowAllDocuments = function (category) {
        $cookies.put("category", category.Id);
        $cookies.put("categoryName", category.Category);
        window.location.href = '#/DocumentList';

    };

    $scope.DeleteDocuments = function (category) {
        var confirm = $mdDialog.confirm()
            .title('Delete Document Category')
            .textContent('Do you really want to delete this Document Category?')
            .ok('Yes')
            .cancel('No');

        $mdDialog.show(confirm).then(function () {
            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/Document/DeleteDocuments",
                data: { categoryId: category.Id },
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                $scope.GetAllDocumentCategories();
                modalService.show("Document Removed", StatusType.SUCCESS);
            });

            get.error(function (data, status) {
                modalService.show("Error Occured", StatusType.ERROR);
            });
        });
    };

});