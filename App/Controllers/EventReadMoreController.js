﻿CMSapp.controller('ReadMoreEventCtrl', function ($scope, Upload, $mdDialog, $mdSidenav, modalService, items) {

    $scope.Name = items.Name;
    $scope.Location = items.Location;
    $scope.Description = items.Description;
    //$scope.Location = items.Location;

    $scope.StartDate = moment(items.StartDate).format("DD/MM/YYYY");
    $scope.file = items.Image;
    $scope.EventId = items.EventId

    $scope.Cancel = function () {
        $mdDialog.cancel();
    };

    $scope.Answer = function (status) {
        $mdDialog.hide("success");
    }
});