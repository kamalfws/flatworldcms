﻿CMSapp.controller('DocumentListController', function ($http, $scope, $location, $mdSidenav, $mdDialog, $cookies, modalService, Auth) {

    $scope.filterBy = '';
    $scope.pageSizeList = getPageSizeList();
    $scope.pPageSizeObj = PageSizeSelectedObj;
    $scope.PageSize = $scope.pPageSizeObj.selected;

    if (Auth.userHasPermission('Administrator'))
        $scope.show_fields = ['DocumentType', 'Name', 'Download', 'Delete'];
    else
        $scope.show_fields = ['DocumentType', 'Name','Download'];
    $scope.UserId = 0;
    $scope.TableType = 'Documents';
    $scope.Id = 0;
    $scope.Category = $cookies.get('categoryName');

    //Called from on-data-required directive.
    $scope.onServerSideItemsRequested = function (currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        LoadUserData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse);
    }

    // Called from global search box
    $scope.onChange = function () {
        LoadUserData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Called from page size drop down changed.
    $scope.changePageSize = function () {
        $scope.PageSize = $scope.pPageSizeObj.selected;
    }

    $scope.clearSearchParams = function () {
        $scope.filterBy = '';
        LoadUserData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Ajax call  to fetch data from server...
    function LoadUserData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        var userSearch = {
            currentPage: currentPage,
            pageItems: pageItems,
            filterBy: filterBy,
            filterByFields: $cookies.get('category'),
            orderBy: orderBy,
            orderByReverse: orderByReverse
        }
        pCurrentPage = currentPage;
        pPageItems = pageItems;
        pOrderBy = orderBy;
        pOrderByReverse = orderByReverse
        var req = {
            method: 'POST',
            url: getBaseUrl() + '/Document/GetDocuments?nocache=' + new Date().getTime(),
            contentType: "undefined",
            data: JSON.stringify(userSearch)
        }
        $http(req).success(function (data) {
            $scope.Documents = data;
            $scope.TotalRecordsCount = data[0].TotalCount;

        }).error(function (error) {
            modalService.show(StatusType.ERROR);
        });
    }

    //Clear all User Input fields
    $scope.clearUserFields = function () {
        $scope.UserId = $scope.FirstName = $scope.LastName = $scope.LoginId = $scope.Mobile = $scope.EmailId = $scope.RoleCode = $scope.UserPassword = $scope.ConfirmPassword = '';
        $scope.Id = 0;

        $scope.Department = 'Select';
        $scope.Submitted = false;
        $scope.EditUserIsLoggedInUser = false;
        $scope.UserInfo.$setPristine();
        $scope.UserInfo.$setUntouched();
    }






    $scope.DeleteDocument = function (gridItem, ev) {
        // Appending dialog to document.body to cover sidenav in docs app

        var confirm = $mdDialog.confirm()
            .title('Delete Document')
            .textContent('Do you really want to delete this Document?')
            .ok('Yes')
            .cancel('No');

        $mdDialog.show(confirm).then(function () {
            var req = {
                method: 'POST',
                url: getBaseUrl() + '/Document/DeleteSingleDocuments?nocache=' + new Date().getTime(),
                contentType: "application/json",
                data: {
                    docId: gridItem.DocumentID
                }
            }
            $http(req).success(function (data) {
                if (data == "Success") {
                    LoadUserData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);

                    modalService.show('Document is deleted successfully', StatusType.SUCCESS);

                }

                else {
                    modalService.show('Document Deleted' + 'FailedMsg', StatusType.ERROR);
                }
            }).error(function (error) {
                modalService.show('ErrorMsgWhileLoading', StatusType.ERROR);
            });
        });

    }

    $scope.DownloadDocument = function (gridItem, ev) {

        var req = {
            method: 'POST',
            url: getBaseUrl() + '/Document/CheckFile?nocache=' + new Date().getTime(),
            contentType: "application/json",
            data: { filePath: gridItem.Path }
        }
        $http(req).success(function (data) {
            if (data == "true") {
                window.location.href = getBaseUrl() + '/Document/Download?filePath=' + gridItem.Path + "&fileName=" + gridItem.Name;
            }
            else {
                modalService.show("File Not Found");
            }


        }).error(function (error) {
            modalService.show(StatusType.ERROR);
        });

    }

    $scope.ShowAddDocument = function () {
        $mdDialog.show({
            controller: 'AddMultipleDocumentCtrl',
            templateUrl: getBaseUrl() + '/App/Partials/AddMultiDocument.html?nocache=' + new Date().getTime(),
            parent: angular.element(document.body),
            // targetEvent: ev,
             clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function (answer) {
            if (answer.toLowerCase() == "success") {
                LoadUserData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
                modalService.show("Successfully Uploaded", StatusType.SUCCESS);
            }
            else
                modalService.show("Error Occured", StatusType.ERROR);
            //$scope.status = 'You said the information was "' + answer + '".';
        }, function () {

        });
    };

    //to display document type icon
    $scope.getDocumentType = function (documentType){
        if (documentType == "Image")
            return "fa fa-picture-o fa-2x";
        else if (documentType == "Video")
            return "fa fa-file-video-o fa-2x";
        else if (documentType == "Audio")
            return "fa fa-file-audio-o fa-2x";
        else if (documentType == "Word")
            return "fa fa-file-word-o fa-2x";
        else if (documentType == "Excel")
            return "fa fa-file-excel-o fa-2x";
        else if (documentType == "PowerPoint")
            return "fa fa-file-powerpoint-o fa-2x";
        else if (documentType == "Text")
            return "fa fa-file-text-o fa-2x";
        else if (documentType == "Zip")
            return "fa fa-file-archive-o fa-2x";
        else if (documentType == "Pdf")
            return "fa fa-file-pdf-o fa-2x";
        else
            return "fa fa-file fa-2x";

    }
});