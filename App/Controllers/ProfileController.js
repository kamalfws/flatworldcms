﻿

CMSapp.controller('ProfileCtrl', function ($http, $scope, $location, modalService, Upload) {
    $scope.LoadUserDetails = function (userId) {
        var req = {
            method: 'POST',
            url: getBaseUrl() + '/User/GetSingleUser?nocache=' + new Date().getTime(),
            contentType: "application/json",
            data: {
                userId: userId
            }
        }
        $http(req).success(function (data) {

            $scope.RoleIndex = -1;
            $scope.DepartmentIndex = -1;
            $scope.DesignationIndex = -1;
            $scope.roles = data.roles;
            $scope.departments = data.departments;
            $scope.designations = data.designations;
            $scope.FirstName = data.user.FirstName;
            $scope.LastName = data.user.LastName;
            $scope.RoleId = data.user.Role;
            $scope.user = data.user;
            $scope.Id = data.user.UserId;
            $scope.Address = data.user.ContactAddress;
            $scope.ProfilePicPath = data.user.ProfilePicPath;

            angular.forEach($scope.roles, function (role, index) {
                //Just add the index to your item
                if (role.RoleDescription.toLowerCase() == data.user.Role.toLowerCase()) {
                    $scope.RoleIndex = index;
                }
            });

            $scope.Department = data.user.Department;

            angular.forEach($scope.departments, function (department, index) {
                //Just add the index to your item
                if (department.Name.toLowerCase() == data.user.Department.toLowerCase()) {
                    $scope.DepartmentIndex = index;
                }
            });

            $scope.Designation = data.user.Designation;

            angular.forEach($scope.designations, function (designation, index) {
                //Just add the index to your item
                if (designation.Name.toLowerCase() == data.user.Designation.toLowerCase()) {
                    $scope.DesignationIndex = index;
                }
            });

            $scope.Email = data.user.Email;
            $scope.Active = data.user.Active;
            $scope.Mobile = data.user.Mobile;
            $scope.DateofBirth = new Date(moment(data.user.DateofBirth).format("DD-MMM-YYYY"));
            $scope.DateofJoining = new Date(moment(data.user.DateofJoining));
            $scope.ReportingManager = data.user.ReportingManager;
            $scope.Gender = data.user.Gender;

        }).error(function (error) {
            modalService.show('Error While Fetching Data', StatusType.ERROR);
        });

    }

    $scope.SaveUser = function () {
        //var details = { "Id": $scope.Id, "FirstName": $scope.FirstName, "LastName": $scope.LastName, "Email": $scope.Email, "Mobile": $scope.Mobile, "DOB": $scope.DateofBirth, "DOJ": $scope.DateofJoining, "Manager": $scope.ReportingManager, "Gender": $scope.Gender };
        if ($scope.UserInfo.$valid) {

            Upload.upload({
                method: "POST",
                url: getBaseUrl() + '/User/UpdateProfile',
                data: { "Id": $scope.Id, "FirstName": $scope.FirstName, "LastName": $scope.LastName, "Mobile": $scope.Mobile, "DOB": $scope.DateofBirth, "Address": $scope.Address, "ProfilePicFile": $scope.profilePicFile }
            }).then(function (resp) {
                modalService.show('Success', StatusType.SUCCESS);
                $scope.LoadUserDetails($scope.Id);
            }, function (resp) {
                modalService.show('Failure', StatusType.ERROR);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            });

            debugger;
            //var get = $http({
            //    method: "POST",
            //    url: getBaseUrl() + "/User/UpdateProfile",
            //    data: { "Id": $scope.Id, "FirstName": $scope.FirstName, "LastName": $scope.LastName, "Mobile": $scope.Mobile, "DOB": $scope.DateofBirth, "Address": $scope.Address, "ProfilePicFile": $scope.profilePicFile }
            //});

            //get.success(function (data, status) {
            //    debugger;
            //    if (data == "success") {
            //        modalService.show('Success', StatusType.SUCCESS);
            //        // LoadUserData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
            //    }
            //    else if (data == "error") {
            //        modalService.show('Failure', StatusType.ERROR);
            //    }

            //    else
            //        modalService.show(data, StatusType.ERROR);
            //});

            //get.error(function (data, status) {
            //    modalService.show('Failure', StatusType.ERROR);
            //});
        }
    }

    $scope.LoadUserDetails($scope.loggedInUser.Id);
});