﻿
CMSapp.controller('BirthdayCtrl', function ($scope, $http, $filter) {

    $scope.currentMonth = $filter('date')(new Date(), 'MMMM');
    $scope.GetAllBirthDays = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/User/GetBirthdays",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            $scope.birthdays = data.birthdays;
            $scope.year = data.year;
            $scope.GetVisibility($scope.currentMonth);
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    }
    $scope.$watch('currentMonth', function (val) {
        $scope.GetVisibility(val);
    });
    $scope.GetVisibility = function (val) {
        var items = $filter('filter')($scope.birthdays, val);
        $scope.hasData = items.length > 0 ? true : false;
    }
    $scope.GetAllBirthDays();
});


