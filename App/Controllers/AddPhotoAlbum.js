﻿
CMSapp.controller('AddPhotoAlbumCtrl', function ($scope, Upload, $mdDialog, $mdSidenav, modalService) {
  
    
    $scope.SaveAlbum = function () {
        if ($scope.form.file.$valid && $scope.file) {
            $scope.upload($scope.file);

        }
        else
            modalService.show("Please Upload Cover Photo", StatusType.ERROR);
    }
    // upload on file select or drop
    $scope.upload = function (file) {
        Upload.upload({
            url: getBaseUrl() + '/Photo/SavePhotoAlbum',
            data: { 'file': file, 'Name': $scope.Name,'photos':$scope.files }
        }).then(function (resp) {
            $scope.Answer("Success");
        }, function (resp) {
            $scope.Answer("Failure");
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        });
    };

    $scope.Cancel = function () {
        $mdDialog.cancel();
    };

    $scope.Answer = function (status) {
        $mdDialog.hide("success");
    }

 

});


