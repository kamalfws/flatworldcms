﻿
CMSapp.controller('AddDocumentCtrl', function ($http,$scope, Upload, $mdDialog, $mdSidenav, modalService) {
  
    
    $scope.SaveDocument = function () {
        if ( $scope.file) {
            $scope.upload($scope.file);
        }
        else
            modalService.show("Please Upload Valid Files", StatusType.ERROR);
    }
    // upload on file select or drop
    $scope.upload = function (file) {
       
            Upload.upload({
                url: getBaseUrl() + '/Document/SaveDocument',
                data: { 'file': file, 'title': $scope.docCategory }
            }).then(function (resp) {
                $scope.Answer("Success");
            }, function (resp) {
                $scope.Answer("Failure");
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            });
       
        
    };

    $scope.Cancel = function () {
        $mdDialog.cancel();
    };

  
    $scope.Answer = function (status) {
        $mdDialog.hide("success");
    }

     $scope.CheckForDuplicateCategory = function () {
       
         if ($scope.docCategory != "" && $scope.docCategory!=undefined) {
             var get = $http({
                 method: "POST",
                 url: getBaseUrl() + "/Document/CheckCategory",
                 data: { name: $scope.docCategory },
                 dataType: 'json',
                 headers: { "Content-Type": "application/json" }
             });

             get.success(function (data) {
                 if (data == "True") {

                 }
                 else {
                     modalService.show("Duplicate Category", StatusType.Error);
                     $scope.docCategory = "";

                 }
             });

             get.error(function (data, status) {
                 modalService.show("Error Occured", StatusType.ERROR);
             });
         }
         else { modalService.show("Category name is empty", StatusType.ERROR);}

    };


});


