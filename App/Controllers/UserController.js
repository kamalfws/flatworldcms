﻿CMSapp.controller('UserController', function ($http, $scope, $location, $mdSidenav, $mdDialog, modalService, Auth) {
    $scope.filterBy = '';
    $scope.pageSizeList = getPageSizeList();
    $scope.pPageSizeObj = PageSizeSelectedObj;
    $scope.PageSize = $scope.pPageSizeObj.selected;
   if (Auth.userHasPermission('Administrator'))
        $scope.show_fields = ['UserId', 'Name', 'Role', 'Department', 'Designation', 'Active'];
    else
        $scope.show_fields = ['UserId', 'Name', 'Role', 'Department', 'Designation'];

    $scope.UserId = 0;
    $scope.TableType = 'User';
    $scope.Id = '';
    $scope.DateofBirthMinDate = new Date(moment().subtract(60, 'years'));
    $scope.DateofBirthMaxDate = new Date(moment().subtract(18, 'years'));
    $scope.UserDateofBirthMaxDate = moment().subtract(18, 'years').format("DD/MM/YYYY");
    $scope.DateofJoiningMinDate = new Date("01-Apr-2002");
    $scope.DateofJoiningMaxDate = new Date();

    //Called from on-data-required directive.
    $scope.onServerSideItemsRequested = function (currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        LoadUserData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse);
    }

    // Called from global search box
    $scope.onChange = function () {
        LoadUserData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Called from page size drop down changed.
    $scope.changePageSize = function () {
        $scope.PageSize = $scope.pPageSizeObj.selected;
    }

    $scope.clearSearchParams = function () {
        $scope.filterBy = '';
        LoadUserData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Ajax call  to fetch data from server...
    function LoadUserData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        var userSearch = {
            currentPage: currentPage,
            pageItems: pageItems,
            filterBy: filterBy,
            filterByFields: angular.toJson(filterByFields),
            orderBy: orderBy,
            orderByReverse: orderByReverse
        }
        pCurrentPage = currentPage;
        pPageItems = pageItems;
        pOrderBy = orderBy;
        pOrderByReverse = orderByReverse
        var req = {
            method: 'POST',
            url: getBaseUrl() + '/User/GetUsers?nocache=' + new Date().getTime(),
            contentType: "application/json",
            data: JSON.stringify(userSearch)
        }
        $http(req).success(function (data) {
            $scope.Users = data;
            $scope.TotalRecordsCount = data[0].TotalCount;

        }).error(function (error) {
            modalService.show(StatusType.ERROR);
        });
    }

    //Clear all User Input fields
    $scope.clearUserFields = function () {
        $scope.UId = '';

        $scope.FirstName = '';
        $scope.LastName = '';
        $scope.RoleId = '';
        $scope.Email = '';
        $scope.Mobile = '';
        $scope.ReportingManager = '';


        $scope.DateofBirth = '';
        $scope.DateofJoining = '';

        $scope.RoleId = '';
        $scope.Department = '';
        $scope.Designation = '';

        $scope.RoleIndex = -1;
        $scope.DepartmentIndex = -1;
        $scope.DesignationIndex = -1;
        $scope.ManagerIndex = -1;

        //$scope.UserInfo.$setPristine();
        //$scope.UserInfo.$setUntouched();

    }

    $scope.LoadUserDetails = function (userId) {
        var req = {
            method: 'POST',
            url: getBaseUrl() + '/User/GetSingleUser?nocache=' + new Date().getTime(),
            contentType: "application/json",
            data: {
                userId: userId
            }
        }
        $http(req).success(function (data) {
            debugger;
            $scope.RoleIndex = -1;
            $scope.DepartmentIndex = -1;
            $scope.DesignationIndex = -1;
            $scope.roles = data.roles;
            $scope.Managers = data.managers;
            $scope.departments = data.departments;
            $scope.designations = data.designations;
            if (data.user != null) {
                $scope.FirstName = data.user.FirstName;
                $scope.LastName = data.user.LastName;
                $scope.RoleId = data.user.Role;
                $scope.user = data.user;
                $scope.UId = data.user.UserId;
             
                angular.forEach($scope.roles, function (role, index) {
                    //Just add the index to your item
                    if (role.RoleDescription.toLowerCase() == data.user.Role.toLowerCase()) {
                        $scope.RoleIndex = index;
                    }
                });

                $scope.Department = data.user.Department;
                angular.forEach($scope.departments, function (department, index) {
                    //Just add the index to your item
                    if (department.Name.toLowerCase() == data.user.Department.toLowerCase()) {
                        $scope.DepartmentIndex = index;
                    }
                });

                $scope.Designation = data.user.Designation;
                angular.forEach($scope.designations, function (designation, index) {
                    //Just add the index to your item
                    if (designation.Name.toLowerCase() == data.user.Designation.toLowerCase()) {
                        $scope.DesignationIndex = index;
                    }
                });

                angular.forEach($scope.Managers, function (manager, index) {
                    //Just add the index to your item
                    if (manager.Name.toLowerCase() == data.user.ReportingManager.toLowerCase()) {
                        $scope.ManagerIndex = index;
                    }
                });

                $scope.Email = data.user.Email;
                $scope.Active = data.user.Active;
                $scope.Mobile = data.user.Mobile;
                $scope.DateofBirth = new Date(moment(data.user.DateofBirth));
                $scope.DateofJoining = new Date(moment(data.user.DateofJoining));
                $scope.ReportingManager = data.user.ReportingManager;
                $scope.Gender = data.user.Gender;
            }

            $mdDialog.show({
                controller: 'UserController',
                templateUrl: getBaseUrl() + '/App/Partials/AddUser.html?nocache=' + new Date().getTime(),
                parent: angular.element(document.body),
                scope: $scope,
                preserveScope: true,
                clickOutsideToClose: false,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })

        }).error(function (error) {
            modalService.show('Error While Fetching Data', StatusType.ERROR);
        });

    }

    $scope.ShowPopup = function (isNew, grd) {
        $scope.isNew = isNew;
        $scope.clearUserFields();
        $scope.LoadUserDetails(grd != undefined ? grd.UserId : 0);
       
    }

    $scope.SaveUser = function () {
        //var details = { "Id": $scope.Id, "FirstName": $scope.FirstName, "LastName": $scope.LastName, "Email": $scope.Email, "Mobile": $scope.Mobile, "DOB": $scope.DateofBirth, "DOJ": $scope.DateofJoining, "Manager": $scope.ReportingManager, "Gender": $scope.Gender };
        if ($scope.UserInfo.$valid) {
            if ($scope.Email.indexOf("@flatworldsolutions.com") == -1) {
                modalService.show("Invalid Email Id domain, Eg. 'david@flatworldsolutions.com'", StatusType.ERROR);
                return;
            }
            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/User/SaveUser",
                data: { "Id": $scope.UId, "FirstName": $scope.FirstName, "LastName": $scope.LastName, "Email": $scope.Email, "Mobile": $scope.Mobile, "DOB": $scope.DateofBirth, "DOJ": $scope.DateofJoining, "Manager": $scope.ReportingManager, "Gender": $scope.Gender, "Department": $scope.Department, "Designation": $scope.Designation, "Role": $scope.RoleId, "isNew": $scope.isNew },
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                if (data == "success") {
                    modalService.show($scope.isNew ? 'User Added Successfully' : 'User Updated Successfully', StatusType.SUCCESS);
                    $scope.closePopup();
                    LoadUserData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
                    //LoadUserData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse);
                }
                else if (data == "error") {
                    modalService.show('Failure', StatusType.ERROR);
                }

                else
                    modalService.show(data, StatusType.ERROR);
            });

            get.error(function (data, status) {
                modalService.show('Failure', StatusType.ERROR);
            });
        }
    }

    $scope.closePopup = function () {
        $mdDialog.hide();
    }

    $scope.IsAdmin = function () {
        return Auth.userHasPermission("Administrator");
    }

    $scope.showEnableDisableConfirm = function (gridItem, ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var original = !gridItem.Active;
        var confirm = $mdDialog.confirm()
            .title((gridItem.Active == false ? 'Disable' : 'Enable') + ' ' + 'User')
            .textContent(' Are you sure, you want to' + ' ' + (gridItem.Active == false ? 'Disable' : 'Enable') + ' this User?')
            .ok('Yes')
            .cancel('No');

        $mdDialog.show(confirm).then(function () {
            var req = {
                method: 'POST',
                url: getBaseUrl() + '/User/EnableDisableUser?nocache=' + new Date().getTime(),
                contentType: "application/json",
                data: {
                    userId: gridItem.UserId,
                    enable: gridItem.Active
                }
            }
            $http(req).success(function (data) {
                if (data == "Success") {
                    modalService.show('User Is ' + (gridItem.Active == false ? 'Disabled' : 'Enabled'), StatusType.SUCCESS);
                }
                else if (data == "OnlyOneAdmin") {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .title('Alert')
                        .textContent('UserDisableWarning')
                        .ok('Ok')
                    );
                    gridItem.Active = original;
                }
                else {
                    modalService.show('User Is' + (gridItem.Active == false ? 'Disable' : 'Enable') + 'FailedMsg', StatusType.ERROR);
                    gridItem.Active = original;
                }
            }).error(function (error) {
                modalService.show('ErrorMsgWhileLoading', StatusType.ERROR);
            });
        }, function () {
            gridItem.Active = original;
        });
    };

    $scope.ShowUploadPopup = function () {
        $mdDialog.show({
            contentElement: '#usersUploadDialog',
            parent: angular.element(document.body)
        });
    }
});