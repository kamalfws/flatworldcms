﻿CMSapp.controller('PhotoGalleryController', function ($http, $scope, $location, $mdSidenav, $mdDialog,$cookies,modalService,Lightbox) {
    debugger;
    $scope.albumName = $cookies.get('photoAlbumName');
    $scope.albumID=$cookies.get('photoAlbumId') ;
     var get = $http({
        method: "GET",
        url: getBaseUrl() + "/Photo/GetPhotos?ID=" + $scope.albumID,
       
        dataType: 'json',
        headers: { "Content-Type": "application/json" }
    });

    get.success(function (data, status) {
        $scope.photos = data;

    });

    get.error(function (data, status) {
        modalService.show("Error Occured", StatusType.ERROR);
    });

    $scope.openLightboxModal = function (index) {
        debugger;
        Lightbox.openModal($scope.photos, index);
    };
    $scope.GetPhotos = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/Photo/GetPhotos?ID=" + $scope.albumID,
           
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            $scope.photos = data;

        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });

    };


    $scope.ShowAddPhotos= function () {
        $mdDialog.show({
            controller: 'AddPhotosController',
            templateUrl: getBaseUrl() + '/App/Partials/AddPhotos.html',
            parent: angular.element(document.body),
            // targetEvent: ev,
             clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function (answer) {
            if (answer.toLowerCase() == "success") {
                $scope.GetPhotos();
                modalService.show("Successfully Uploaded", StatusType.SUCCESS);
            }
            else
                modalService.show("Error Occured", StatusType.ERROR);
            //$scope.status = 'You said the information was "' + answer + '".';
        }, function () {

        });
    };

    $scope.DeletePhoto = function (photo) {
        var confirm = $mdDialog.confirm()
         .title('Delete Photo')
         .textContent('Do you want to delete this Photo?')
         .ok('Yes')
         .cancel('No');

        $mdDialog.show(confirm).then(function () {
            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/Photo/Deletephoto",
                data: { photoId: photo.Id },
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                $scope.GetPhotos();
                modalService.show("Photo Removed", StatusType.SUCCESS);
            });

            get.error(function (data, status) {
                modalService.show("Error Occured", StatusType.ERROR);
            });
        });
    };


    });