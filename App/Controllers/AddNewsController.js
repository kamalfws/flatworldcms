﻿CMSapp.controller('AddNewsCtrl', function ($scope, $http, $mdDialog, modalService, Auth, $sce) {
    $scope.SelectedCategory = 'Formal';
    $scope.GetNewsCategories = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/News/GetCategories",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            $scope.categories = data;
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    };

    $scope.SetData = function () {
        if ($scope.editedNewsId != undefined && $scope.editedNewsId != 0) {
            $scope.title = $scope.editedNewsTitle;
            angular.forEach($scope.categories, function (category, index) {
                //Just add the index to your item
                if (category.Id == $scope.editedNewsCategoryId) {
                    $scope.SelectedCategoryIndex = index;
                    $scope.SelectedCategory = '' + $scope.editedNewsCategoryId;
                }
            });
            $scope.description = $scope.editedNewsDescription;
        }
        else {
            $scope.title = '';
            $scope.SelectedCategoryIndex = -1;
            $scope.SelectedCategory = undefined;
            $scope.description = '';
        }
    };

    //$scope.SelectCategory = function (Id, index) {
    //    $scope.SelectedCategory = Id;
    //    $scope.tab = index;
    //};

    //$scope.tabIsSet = function (tabId) {
    //    return $scope.tab === tabId;
    //};

    //$tab = 0;

    $scope.SaveNews = function () {
        description = tinyMCE.activeEditor.getContent();
        if (!ValidateCharacterLength()) {
            modalService.show("Maximum allowed characters for editor is 3000", StatusType.ERROR);
            return;
        }

        if ($scope.newsForm.$valid) {
            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/News/SaveNews",
                data: {
                    "categoryId": $scope.SelectedCategory,
                    "title": $scope.title, 
                    "description": description,
                    "id": $scope.editedNewsId,
                    "file":files
                },
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                if (data == "success") {
                     $scope.GetNews();
                    $mdDialog.hide();
                    modalService.show("Successfully " + ($scope.editedNewsId==0?"Added":"Updated")+" News", StatusType.SUCCESS);
                 
                }
                else {
                    $scope.Answer("Failure");
                }
                $mdDialog.hide(status);
            });

            get.error(function (data, status) {
                $scope.Answer("Failure");
            });
        }
    };

    $scope.Cancel = function () {
        $scope.Answer("Cancel");
        if (operationType == "Add") {
            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/News/DeleteUnsavedNewsImages",
                dataType: 'json',
                data: {
                    images: newsImages
                },
            });
            get.success(function (data, status) {
                
            });
            get.error(function (data, status) {
                modalService.show("Error Occured", StatusType.ERROR);
            });
        }
    };

    $scope.Answer = function (status) {
        $mdDialog.hide(status);
    }

    $scope.IsAdmin = function () {
        return Auth.userHasPermission("Administrator");
    }

    $scope.GetNewsCategories();
    $scope.SetData();
});

//count number of characters in the tinymce html editor
function CountCharacters() {
    var body = tinymce.get("txtNewsEditor").getBody();
    var content = tinymce.trim(body.innerText || body.textContent);
    return content.length;
};

//restrict user to enter only 3000 characters in tinymce editor
function ValidateCharacterLength() {
    var max = 5000;
    var count = CountCharacters();
    if (count > max) {
        return false;
    }
    return true;
}