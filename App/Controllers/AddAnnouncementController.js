﻿CMSapp.controller('AddAnnouncementCtrl', function ($scope, $http, $mdDialog, modalService, Auth) {

    $scope.GetAnnouncementCategories = function () {
        var get = $http({
            method: "GET",
            url: getBaseUrl() + "/Announcement/GetCategories",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        });

        get.success(function (data, status) {
            $scope.categories = data;
        });

        get.error(function (data, status) {
            modalService.show("Error Occured", StatusType.ERROR);
        });
    };

    $scope.SetData = function () {
        if ($scope.editedAnnouncementId != undefined && $scope.editedAnnouncementId != 0) {
            $scope.title = $scope.editedAnnouncementTitle;
            angular.forEach($scope.categories, function (category, index) {
                //Just add the index to your item
                if (category.Id == $scope.editedAnnouncementCategoryId) {
                    $scope.SelectedCategoryIndex = index;
                    $scope.SelectedCategory = '' + $scope.editedAnnouncementCategoryId;
                }
            });
            $scope.description = $scope.editedAnnouncementDescription;
        }
        else {
            $scope.title = '';
            $scope.SelectedCategoryIndex = -1;
            $scope.SelectedCategory = undefined;
            $scope.description = '';
        }
    };

    $scope.IsAdmin = function () {
        return Auth.userHasPermission("Administrator");
    }

    //$scope.SelectCategory = function (Id, index) {
    //    $scope.SelectedCategory = Id;
    //    $scope.tab = index;
    //};

    //$scope.tabIsSet = function (tabId) {
    //    return $scope.tab === tabId;
    //};

    //$tab = 0;

    $scope.SaveAnnouncement = function () {
        description = tinyMCE.activeEditor.getContent();
        if (!ValidateCharacterLength()) {
            modalService.show("Maximum allowed characters for editor is 5000", StatusType.ERROR);
            return;
        }
        if ($scope.announcementForm.$valid) {
        debugger;
       
      
        var cat=$scope.SelectedCategory;
            var get = $http({
                method: "POST",
                url: getBaseUrl() + "/Announcement/SaveAnnouncement",
                data: { "categoryId": 1, "title": $scope.title, "description": description, "id": $scope.editedAnnouncementId },
                dataType: 'json',
                headers: { "Content-Type": "application/json" }
            });

            get.success(function (data, status) {
                if (data == "success") {
                    $scope.GetAnnouncements();
                    $mdDialog.hide();
                   modalService.show("Successfully " + ($scope.editedNewsId==0?"Added":"Updated")+" News", StatusType.SUCCESS);
                }
                else {
                    $scope.Answer("Failure");
                }
            });

            get.error(function (data, status) {
                $scope.Answer("Failure");
            });
        }
    };

    $scope.Cancel = function () {
        $scope.Answer("Cancel");
        // $mdDialog.cancel();
    };

    $scope.Answer = function (status) {
        $mdDialog.hide(status);
    }

    //$scope.GetAnnouncementCategories();
    $scope.SetData();

    //count number of characters in the tinymce html editor
    function CountCharacters() {
        var body = tinymce.get("txtAnnouncementEditor").getBody();
        var content = tinymce.trim(body.innerText || body.textContent);
        return content.length;
    };

    //restrict user to enter only 3000 characters in tinymce editor
    function ValidateCharacterLength() {
        var max = 5000;
        var count = CountCharacters();
        if (count > max) {
            return false;
        }
        return true;
    }
});

