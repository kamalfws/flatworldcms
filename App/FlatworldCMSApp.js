﻿//1- page should not be load durig video upload.

//FlatworldCMS Angular CMSapp
// CMSapp Module: the name AngularStore matches the ng-app attribute in the main <html> tag
// the route provides parses the URL and injects the appropriate partial page
//var CMSapp = angular.module('FlatworldCMS', [
//   'ngCookies', 'ngRoute', 'bootstrapLightbox', 'LocalStorageModule', 'ngSanitize']);
var CMSapp = angular.module('FlatworldCMS', ['ngCookies', 'ngRoute', 'bootstrapLightbox', 'LocalStorageModule', 'ngMaterial', 'ngFileUpload', 'trNgGrid', 'ngMessages', 'AuthServices', 'ngSanitize', 'ngLoadingSpinner', 'angularLivestamp'])
    .directive('permission', ['Auth', function (Auth) {
        return {
            restrict: 'A',
            scope: {
                permission: '='
            },

            link: function (scope, elem, attrs) {
                scope.$watch(Auth.isLoggedIn, function () {
                    if (Auth.userHasPermission(scope.permission)) {
                        elem.show();
                    } else {
                        elem.hide();
                    }
                });
            }
        }
    }])
    .run(['$rootScope', 'Auth', '$window','$mdDialog', function ($rootScope, Auth, $window, $mdDialog) {
        Auth.init();

        $rootScope.$on('$routeChangeStart', function (event, next) {
            if (!Auth.checkPermissionForView(next)) {
                event.preventDefault();
                $window.location = "/Account/Login";
            }
        });

        $rootScope.$on('$locationChangeStart', function ($event) {
            // Check if there is a dialog active
            $mdDialog.cancel();  // Cancel the active dialog
        });
    }]);

//Configuration Section
CMSapp.config(['$routeProvider', '$locationProvider', '$mdDateLocaleProvider',
    function ($routeProvider, $locationProvider, $mdDateLocaleProvider) {

        $routeProvider.
            when('/VideoGallery', {
                templateUrl: '/App/Partials/VideoGallery.html?nocache=' + new Date().getTime(),
                controller: 'VideoGalleryCtrl',
                activetab: 'Video',
                requiresAuthentication: true
            })
            .when('/VideoAlbums', {

                templateUrl: '/App/Partials/VideoAlbums.html?nocache=' + new Date().getTime(),
                controller: 'VideoAlbumsController',
                activetab: 'Video',
                requiresAuthentication: true
            }).
            when('/PhotoGallery', {
                templateUrl: '/App/Partials/PhotoGallery.html?nocache=' + new Date().getTime(),
                controller: 'PhotoGalleryController',
                activetab: 'Photo',
                requiresAuthentication: true
            })
            .when('/PhotoAlbum', {
                templateUrl: '/App/Partials/PhotoAlbum.html?nocache=' + new Date().getTime(),
                controller: 'PhotoAlbumsController',
                activetab: 'Photo',
                requiresAuthentication: true
            })
            .when('/Dashboard', {
                templateUrl: '/App/Partials/Dashboard.html?nocache=' + new Date().getTime(),
                controller: 'DashboardCtrl',
                activetab: 'Dashboard'
                //,
                //requiresAuthentication: true
            }).when('/Users', {

                templateUrl: '/App/Partials/UserList.html?nocache=' + new Date().getTime(),
                controller: 'UserController',
                activetab: 'Users',
                requiresAuthentication: true
            }).when('/Banners', {
                templateUrl: '/App/Partials/Banners.html?nocache=' + new Date().getTime(),
                controller: "BannerCtrl",
                activetab: 'Banners',
                requiresAuthentication: true,
                permission: "Administrator"
            }).when('/Birthdays', {
                templateUrl: '/App/Partials/Birthdays.html?nocache=' + new Date().getTime(),
                controller: 'BirthdayCtrl',
                activetab: 'Birthdays',
                requiresAuthentication: true
            })
            .when('/Documents', {

                templateUrl: '/App/Partials/DocumentCategory.html?nocache=' + new Date().getTime(),
                controller: 'DocumentController',
                activetab: 'Documents',
                requiresAuthentication: true
            })
            .when('/Projects', {

                templateUrl: '/App/Partials/ProjectList.html?nocache=' + new Date().getTime(),
                controller: 'ProjectController',
                activetab: 'Projects',
                requiresAuthentication: true
            })
            .when('/DocumentList', {

                templateUrl: '/App/Partials/DocumentList.html?nocache=' + new Date().getTime(),
                controller: 'DocumentListController',
                activetab: 'Documents',
                requiresAuthentication: true
            })
            .when('/Announcements', {
                templateUrl: '/App/Partials/Announcements.html?nocache=' + new Date().getTime(),
                controller: 'AnnouncementCtrl',
                requiresAuthentication: true
            })
            .when('/News', {
                templateUrl: '/App/Partials/News.html?nocache=' + new Date().getTime(),
                controller: 'NewsCtrl',
                activetabe: 'News',
                requiresAuthentication: true
            })
            .when('/Events', {
                templateUrl: '/App/Partials/Events.html?nocache=' + new Date().getTime(),
                controller: 'EventsController',
                activetab: 'Events',
                requiresAuthentication: true
            })
            .when('/Profile', {
                templateUrl: '/App/Partials/Profile.html?nocache=' + new Date().getTime(),
                controller: 'ProfileCtrl',
                activetab: 'Users',
                requiresAuthentication: true
            }).when('/PaymentPlan', {
                templateUrl: '/App/Partials/Payment.html?nocache=' + new Date().getTime(),
                controller: 'PaymentCtrl',
                requiresAuthentication: true
               
            })
            .when('/Register', {
                templateUrl: '/Register/Register.cshtml?nocache=' + new Date().getTime(),
                controller: 'Register',
               // requiresAuthentication: true
            })

             .when('/ApplicationSettings', {
                 templateUrl: '/App/Partials/Settings.html?nocache=' + new Date().getTime(),
                 controller: 'ApplicationSettingsController',
                 // requiresAuthentication: true
             })
            .otherwise({ redirectTo: '/Dashboard' });

        $mdDateLocaleProvider.formatDate = function (date) {
            return date ? moment(date).format('DD/MM/YYYY') : '';
        }

        $mdDateLocaleProvider.parseDate = function (dateString) {
            var m = moment(dateString, 'DD/MM/YYYY', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };
    }]);

CMSapp.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        if (input != undefined)
            return input.slice(start);
        else
            return null;
    }
});

CMSapp.filter('unsafe', ['$sce', function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };
}]);

CMSapp.config(function (localStorageServiceProvider) {
    localStorageServiceProvider
        .setPrefix('CMSapp')
        .setStorageType('sessionStorage')
        .setNotify(true, true)

});

CMSapp.config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        '*://www.youtube.com/**'
    ]);
});

var StatusType = {
    SUCCESS: 0,
    ERROR: 1,
    WARNING: 2
}

var pCurrentPage = 0; //PageIndex
var pPageItems = 5;   //PageSize  
var pOrderBy = "";    //SortBy
var pOrderByReverse = false;  //SortDirection = 0

var PageSizeSelectedObj = { selected: 10 };

var DefaultDropDownValue = { selected: "Select" };
var errorMessage = '';
var getPageSizeList = function () {
    return [
        { value: 5, text: "5" },
        { value: 10, text: "10" },
        { value: 15, text: "15" },
        { value: 30, text: "30" },
        { value: 50, text: "50" },
        { value: 100, text: "100" }
    ];
}
getBaseUrl = function () {
    if (location.host.indexOf('localhost') >= 0) {
        return location.protocol + "//" + location.host;
    }
    else {
        return location.protocol + "//" + location.host;
    }
}

function closeModalDialog($rootScope, $mdDialog) {
    $rootScope.$on('$stateChangeSuccess',
        function (event, toState, toParams, fromState) {
            //close any open dialogs
            $mdDialog.cancel();
        });
}

